package com.google.gwt.i18n.client.impl.cldr;

public class DateTimeFormatInfoImpl_en_JM_runtimeSelection extends com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl {
  
  private com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl instance;
  
  @Override
  public java.lang.String[] ampms() {
    ensureInstance();
    return instance.ampms();
  }
  
  @Override
  public java.lang.String dateFormat() {
    ensureInstance();
    return instance.dateFormat();
  }
  
  @Override
  public java.lang.String dateFormatFull() {
    ensureInstance();
    return instance.dateFormatFull();
  }
  
  @Override
  public java.lang.String dateFormatLong() {
    ensureInstance();
    return instance.dateFormatLong();
  }
  
  @Override
  public java.lang.String dateFormatMedium() {
    ensureInstance();
    return instance.dateFormatMedium();
  }
  
  @Override
  public java.lang.String dateFormatShort() {
    ensureInstance();
    return instance.dateFormatShort();
  }
  
  @Override
  public java.lang.String dateTime(java.lang.String timePattern, java.lang.String datePattern) {
    ensureInstance();
    return instance.dateTime(timePattern, datePattern);
  }
  
  @Override
  public java.lang.String dateTimeFull(java.lang.String timePattern, java.lang.String datePattern) {
    ensureInstance();
    return instance.dateTimeFull(timePattern, datePattern);
  }
  
  @Override
  public java.lang.String dateTimeLong(java.lang.String timePattern, java.lang.String datePattern) {
    ensureInstance();
    return instance.dateTimeLong(timePattern, datePattern);
  }
  
  @Override
  public java.lang.String dateTimeMedium(java.lang.String timePattern, java.lang.String datePattern) {
    ensureInstance();
    return instance.dateTimeMedium(timePattern, datePattern);
  }
  
  @Override
  public java.lang.String dateTimeShort(java.lang.String timePattern, java.lang.String datePattern) {
    ensureInstance();
    return instance.dateTimeShort(timePattern, datePattern);
  }
  
  @Override
  public java.lang.String[] erasFull() {
    ensureInstance();
    return instance.erasFull();
  }
  
  @Override
  public java.lang.String[] erasShort() {
    ensureInstance();
    return instance.erasShort();
  }
  
  @Override
  public int firstDayOfTheWeek() {
    ensureInstance();
    return instance.firstDayOfTheWeek();
  }
  
  @Override
  public java.lang.String formatDay() {
    ensureInstance();
    return instance.formatDay();
  }
  
  @Override
  public java.lang.String formatHour12Minute() {
    ensureInstance();
    return instance.formatHour12Minute();
  }
  
  @Override
  public java.lang.String formatHour12MinuteSecond() {
    ensureInstance();
    return instance.formatHour12MinuteSecond();
  }
  
  @Override
  public java.lang.String formatHour24Minute() {
    ensureInstance();
    return instance.formatHour24Minute();
  }
  
  @Override
  public java.lang.String formatHour24MinuteSecond() {
    ensureInstance();
    return instance.formatHour24MinuteSecond();
  }
  
  @Override
  public java.lang.String formatMinuteSecond() {
    ensureInstance();
    return instance.formatMinuteSecond();
  }
  
  @Override
  public java.lang.String formatMonthAbbrev() {
    ensureInstance();
    return instance.formatMonthAbbrev();
  }
  
  @Override
  public java.lang.String formatMonthAbbrevDay() {
    ensureInstance();
    return instance.formatMonthAbbrevDay();
  }
  
  @Override
  public java.lang.String formatMonthFull() {
    ensureInstance();
    return instance.formatMonthFull();
  }
  
  @Override
  public java.lang.String formatMonthFullDay() {
    ensureInstance();
    return instance.formatMonthFullDay();
  }
  
  @Override
  public java.lang.String formatMonthFullWeekdayDay() {
    ensureInstance();
    return instance.formatMonthFullWeekdayDay();
  }
  
  @Override
  public java.lang.String formatMonthNumDay() {
    ensureInstance();
    return instance.formatMonthNumDay();
  }
  
  @Override
  public java.lang.String formatYear() {
    ensureInstance();
    return instance.formatYear();
  }
  
  @Override
  public java.lang.String formatYearMonthAbbrev() {
    ensureInstance();
    return instance.formatYearMonthAbbrev();
  }
  
  @Override
  public java.lang.String formatYearMonthAbbrevDay() {
    ensureInstance();
    return instance.formatYearMonthAbbrevDay();
  }
  
  @Override
  public java.lang.String formatYearMonthFull() {
    ensureInstance();
    return instance.formatYearMonthFull();
  }
  
  @Override
  public java.lang.String formatYearMonthFullDay() {
    ensureInstance();
    return instance.formatYearMonthFullDay();
  }
  
  @Override
  public java.lang.String formatYearMonthNum() {
    ensureInstance();
    return instance.formatYearMonthNum();
  }
  
  @Override
  public java.lang.String formatYearMonthNumDay() {
    ensureInstance();
    return instance.formatYearMonthNumDay();
  }
  
  @Override
  public java.lang.String formatYearMonthWeekdayDay() {
    ensureInstance();
    return instance.formatYearMonthWeekdayDay();
  }
  
  @Override
  public java.lang.String formatYearQuarterFull() {
    ensureInstance();
    return instance.formatYearQuarterFull();
  }
  
  @Override
  public java.lang.String formatYearQuarterShort() {
    ensureInstance();
    return instance.formatYearQuarterShort();
  }
  
  @Override
  public java.lang.String[] monthsFull() {
    ensureInstance();
    return instance.monthsFull();
  }
  
  @Override
  public java.lang.String[] monthsFullStandalone() {
    ensureInstance();
    return instance.monthsFullStandalone();
  }
  
  @Override
  public java.lang.String[] monthsNarrow() {
    ensureInstance();
    return instance.monthsNarrow();
  }
  
  @Override
  public java.lang.String[] monthsNarrowStandalone() {
    ensureInstance();
    return instance.monthsNarrowStandalone();
  }
  
  @Override
  public java.lang.String[] monthsShort() {
    ensureInstance();
    return instance.monthsShort();
  }
  
  @Override
  public java.lang.String[] monthsShortStandalone() {
    ensureInstance();
    return instance.monthsShortStandalone();
  }
  
  @Override
  public java.lang.String[] quartersFull() {
    ensureInstance();
    return instance.quartersFull();
  }
  
  @Override
  public java.lang.String[] quartersShort() {
    ensureInstance();
    return instance.quartersShort();
  }
  
  @Override
  public java.lang.String timeFormat() {
    ensureInstance();
    return instance.timeFormat();
  }
  
  @Override
  public java.lang.String timeFormatFull() {
    ensureInstance();
    return instance.timeFormatFull();
  }
  
  @Override
  public java.lang.String timeFormatLong() {
    ensureInstance();
    return instance.timeFormatLong();
  }
  
  @Override
  public java.lang.String timeFormatMedium() {
    ensureInstance();
    return instance.timeFormatMedium();
  }
  
  @Override
  public java.lang.String timeFormatShort() {
    ensureInstance();
    return instance.timeFormatShort();
  }
  
  @Override
  public java.lang.String[] weekdaysFull() {
    ensureInstance();
    return instance.weekdaysFull();
  }
  
  @Override
  public java.lang.String[] weekdaysFullStandalone() {
    ensureInstance();
    return instance.weekdaysFullStandalone();
  }
  
  @Override
  public java.lang.String[] weekdaysNarrow() {
    ensureInstance();
    return instance.weekdaysNarrow();
  }
  
  @Override
  public java.lang.String[] weekdaysNarrowStandalone() {
    ensureInstance();
    return instance.weekdaysNarrowStandalone();
  }
  
  @Override
  public java.lang.String[] weekdaysShort() {
    ensureInstance();
    return instance.weekdaysShort();
  }
  
  @Override
  public java.lang.String[] weekdaysShortStandalone() {
    ensureInstance();
    return instance.weekdaysShortStandalone();
  }
  
  @Override
  public int weekendEnd() {
    ensureInstance();
    return instance.weekendEnd();
  }
  
  @Override
  public int weekendStart() {
    ensureInstance();
    return instance.weekendStart();
  }
  
  private void ensureInstance() {
    if (instance != null) {
      return;
    }
    String locale = com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().getLocaleName();
    if ("en".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en();
      return;
    }
    if ("en_AS".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_AS();
      return;
    }
    if ("en_AU".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_AU();
      return;
    }
    if ("en_BE".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_BE();
      return;
    }
    if ("en_BW".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_BW();
      return;
    }
    if ("en_BZ".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_BZ();
      return;
    }
    if ("en_CA".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_CA();
      return;
    }
    if ("en_GB".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_GB();
      return;
    }
    if ("en_GU".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_GU();
      return;
    }
    if ("en_HK".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_HK();
      return;
    }
    if ("en_IE".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_IE();
      return;
    }
    if ("en_IN".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_IN();
      return;
    }
    if ("en_MH".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_MH();
      return;
    }
    if ("en_MP".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_MP();
      return;
    }
    if ("en_MT".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_MT();
      return;
    }
    if ("en_NA".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_NA();
      return;
    }
    if ("en_NZ".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_NZ();
      return;
    }
    if ("en_PH".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_PH();
      return;
    }
    if ("en_PK".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_PK();
      return;
    }
    if ("en_SG".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_SG();
      return;
    }
    if ("en_TT".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_TT();
      return;
    }
    if ("en_UM".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_UM();
      return;
    }
    if ("en_US".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_US();
      return;
    }
    if ("en_VI".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_VI();
      return;
    }
    if ("en_ZA".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_ZA();
      return;
    }
    if ("en_ZW".equals(locale)) {
      instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_ZW();
      return;
    }
    instance = new com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en_JM();
  }
}
