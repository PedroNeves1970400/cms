package com.google.gwt.user.cellview.client;

public class CellTree_CellTreeMessages_en_US implements com.google.gwt.user.cellview.client.CellTree.CellTreeMessages {
  
  public java.lang.String showMore() {
    return "Show more";
  }
  
  public java.lang.String emptyTree() {
    return "Empty";
  }
}
