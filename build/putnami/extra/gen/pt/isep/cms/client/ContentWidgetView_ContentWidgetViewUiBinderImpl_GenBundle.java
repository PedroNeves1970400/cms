package pt.isep.cms.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.DoNotEmbed;
import com.google.gwt.resources.client.DataResource.MimeType;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.CssResource.Import;

public interface ContentWidgetView_ContentWidgetViewUiBinderImpl_GenBundle extends ClientBundle {
  @Source("uibinder.pt.isep.cms.client.ContentWidgetView_ContentWidgetViewUiBinderImpl_GenCss_style.gss")
  ContentWidgetView_ContentWidgetViewUiBinderImpl_GenCss_style style();

}
