package pt.isep.cms.client;

import com.google.gwt.resources.client.CssResource;

public interface ContentWidgetView_ContentWidgetViewUiBinderImpl_GenCss_style extends CssResource {
  String name();
  String description();
  String contentTitle();
}
