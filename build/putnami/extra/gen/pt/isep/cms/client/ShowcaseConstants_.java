package pt.isep.cms.client;

public class ShowcaseConstants_ implements pt.isep.cms.client.ShowcaseConstants {
  
  public java.lang.String categoryCells() {
    return "Cell Widgets";
  }
  
  public java.lang.String categoryI18N() {
    return "Internationalization";
  }
  
  public java.lang.String categoryLists() {
    return "Lists and Menus";
  }
  
  public java.lang.String categoryOther() {
    return "Other Features";
  }
  
  public java.lang.String categoryPanels() {
    return "Panels";
  }
  
  public java.lang.String categoryPopups() {
    return "Popups";
  }
  
  public java.lang.String categoryTables() {
    return "Tables";
  }
  
  public java.lang.String categoryTextInput() {
    return "Text Input";
  }
  
  public java.lang.String categoryWidgets() {
    return "Widgets";
  }
  
  public java.lang.String categoryEntities() {
    return "Entities";
  }
  
  public java.lang.String categoryServices() {
    return "Services";
  }
  
  public java.lang.String categoryAbout() {
    return "About";
  }
  
  public java.lang.String cwContactsDescription() {
    return "Manage Contacts";
  }
  
  public java.lang.String cwContactsName() {
    return "Contacts";
  }
  
  public java.lang.String cwAddContactDialogCaption() {
    return "Add Contact ";
  }
  
  public java.lang.String cwUpdateContactDialogCaption() {
    return "Update Contact ";
  }
}
