package pt.isep.cms.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ShowcaseResources_en_InlineClientBundleGenerator implements pt.isep.cms.client.ShowcaseResources {
  private static ShowcaseResources_en_InlineClientBundleGenerator _instance0 = new ShowcaseResources_en_InlineClientBundleGenerator();
  private void catI18NInitializer() {
    catI18N = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catI18N",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 10, 10, false, false
    );
  }
  private static class catI18NInitializer {
    static {
      _instance0.catI18NInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catI18N;
    }
  }
  public com.google.gwt.resources.client.ImageResource catI18N() {
    return catI18NInitializer.get();
  }
  private void catListsInitializer() {
    catLists = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catLists",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 10, 10, false, false
    );
  }
  private static class catListsInitializer {
    static {
      _instance0.catListsInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catLists;
    }
  }
  public com.google.gwt.resources.client.ImageResource catLists() {
    return catListsInitializer.get();
  }
  private void catOtherInitializer() {
    catOther = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catOther",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 1, 1, false, false
    );
  }
  private static class catOtherInitializer {
    static {
      _instance0.catOtherInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catOther;
    }
  }
  public com.google.gwt.resources.client.ImageResource catOther() {
    return catOtherInitializer.get();
  }
  private void catPanelsInitializer() {
    catPanels = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catPanels",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 10, 10, false, false
    );
  }
  private static class catPanelsInitializer {
    static {
      _instance0.catPanelsInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catPanels;
    }
  }
  public com.google.gwt.resources.client.ImageResource catPanels() {
    return catPanelsInitializer.get();
  }
  private void catPopupsInitializer() {
    catPopups = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catPopups",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 10, 10, false, false
    );
  }
  private static class catPopupsInitializer {
    static {
      _instance0.catPopupsInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catPopups;
    }
  }
  public com.google.gwt.resources.client.ImageResource catPopups() {
    return catPopupsInitializer.get();
  }
  private void catTablesInitializer() {
    catTables = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catTables",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 10, 10, false, false
    );
  }
  private static class catTablesInitializer {
    static {
      _instance0.catTablesInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catTables;
    }
  }
  public com.google.gwt.resources.client.ImageResource catTables() {
    return catTablesInitializer.get();
  }
  private void catTextInputInitializer() {
    catTextInput = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catTextInput",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage5),
      0, 0, 10, 10, false, false
    );
  }
  private static class catTextInputInitializer {
    static {
      _instance0.catTextInputInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catTextInput;
    }
  }
  public com.google.gwt.resources.client.ImageResource catTextInput() {
    return catTextInputInitializer.get();
  }
  private void catWidgetsInitializer() {
    catWidgets = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "catWidgets",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage6),
      0, 0, 10, 10, false, false
    );
  }
  private static class catWidgetsInitializer {
    static {
      _instance0.catWidgetsInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return catWidgets;
    }
  }
  public com.google.gwt.resources.client.ImageResource catWidgets() {
    return catWidgetsInitializer.get();
  }
  private void gwtLogoInitializer() {
    gwtLogo = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "gwtLogo",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage7),
      0, 0, 61, 50, false, false
    );
  }
  private static class gwtLogoInitializer {
    static {
      _instance0.gwtLogoInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return gwtLogo;
    }
  }
  public com.google.gwt.resources.client.ImageResource gwtLogo() {
    return gwtLogoInitializer.get();
  }
  private void gwtLogoThumbInitializer() {
    gwtLogoThumb = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "gwtLogoThumb",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage8),
      0, 0, 14, 13, false, false
    );
  }
  private static class gwtLogoThumbInitializer {
    static {
      _instance0.gwtLogoThumbInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return gwtLogoThumb;
    }
  }
  public com.google.gwt.resources.client.ImageResource gwtLogoThumb() {
    return gwtLogoThumbInitializer.get();
  }
  private void isepLogoInitializer() {
    isepLogo = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "isepLogo",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage9),
      0, 0, 145, 55, false, false
    );
  }
  private static class isepLogoInitializer {
    static {
      _instance0.isepLogoInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return isepLogo;
    }
  }
  public com.google.gwt.resources.client.ImageResource isepLogo() {
    return isepLogoInitializer.get();
  }
  private void jimmyInitializer() {
    jimmy = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "jimmy",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage10),
      0, 0, 400, 290, false, true
    );
  }
  private static class jimmyInitializer {
    static {
      _instance0.jimmyInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return jimmy;
    }
  }
  public com.google.gwt.resources.client.ImageResource jimmy() {
    return jimmyInitializer.get();
  }
  private void jimmyThumbInitializer() {
    jimmyThumb = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "jimmyThumb",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage11),
      0, 0, 120, 87, false, true
    );
  }
  private static class jimmyThumbInitializer {
    static {
      _instance0.jimmyThumbInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return jimmyThumb;
    }
  }
  public com.google.gwt.resources.client.ImageResource jimmyThumb() {
    return jimmyThumbInitializer.get();
  }
  private void loadingInitializer() {
    loading = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "loading",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage12),
      0, 0, 16, 16, true, false
    );
  }
  private static class loadingInitializer {
    static {
      _instance0.loadingInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return loading;
    }
  }
  public com.google.gwt.resources.client.ImageResource loading() {
    return loadingInitializer.get();
  }
  private void localeInitializer() {
    locale = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "locale",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage13),
      0, 0, 16, 16, false, false
    );
  }
  private static class localeInitializer {
    static {
      _instance0.localeInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return locale;
    }
  }
  public com.google.gwt.resources.client.ImageResource locale() {
    return localeInitializer.get();
  }
  private void cssInitializer() {
    css = new com.google.gwt.resources.client.CssResource() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? (".sc-FixedWidthButton{width:10em}.cw-BasicPopup-thumb{cursor:pointer;cursor:hand}.cw-DictionaryExample-header{color:#7aa5d6;text-decoration:underline;font-weight:bold;padding-right:20px}.cw-DictionaryExample-data{padding-right:20px}.cw-DockPanel td,.cw-FlexTable td{border:1px solid #bbb;padding:3px}.cw-FlexTable-buttonPanel td{border:0}.cw-FlowPanel-checkBox{margin-left:20px}.cw-RedText{color:red}.cw-RichText{border:1px solid #bbb;border-spacing:0}.cw-StackPanelHeader{padding-right:7px;font-weight:bold;font-size:1.4em}") : (".sc-FixedWidthButton{width:10em}.cw-BasicPopup-thumb{cursor:pointer;cursor:hand}.cw-DictionaryExample-header{color:#7aa5d6;text-decoration:underline;font-weight:bold;padding-right:20px}.cw-DictionaryExample-data{padding-right:20px}.cw-DockPanel td,.cw-FlexTable td{border:1px solid #bbb;padding:3px}.cw-FlexTable-buttonPanel td{border:0}.cw-FlowPanel-checkBox{margin-right:20px}.cw-RedText{color:red}.cw-RichText{border:1px solid #bbb;border-spacing:0}.cw-StackPanelHeader{padding-left:7px;font-weight:bold;font-size:1.4em}");
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.google.gwt.resources.client.CssResource get() {
      return css;
    }
  }
  public com.google.gwt.resources.client.CssResource css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAABU0lEQVR42mNgYPjPqBVaz1a/5FBI97oz//deewnEL/53rzv1v2v18ZRJ207w1dfXMzFYhvZxZves+++X2/G/Z9m+/1ef//g/Zdul/wv23fovZxvzv2fT5f8OCfM5GOoW740ECai5pv7P617yf8r2K//7Np7775LcBFYIwpVz9sQz+BdNOgriKDkm/NfxzvxfMnnD/2nbL6MorF108D8DjCNvF/tfwT7+v7JTIliRlmcGHIP4DDBFIAUg2iy8BCxhFFgIpqvn7gaaeOg/Q3L7sv9KQEUgDebh5f8toyrgJjYu2PO/b9P5/0tPvf7PULtoT4R3bs9/Q6AJrmmtYAUWQA0gxa2rTv5vX3fuf8uKIxYMerHd3N0bLvz3yu7+H1wyBawou2f1/7bVJ//XLTnyv2rJsf+gsGYAE8AA7Vp/Rrln04X/VTO3gXHbquNAfCIEqogBAJFK2/Pq2IaRAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAASklEQVR42mMobp/9H4Rji7vwYgaQoqcffoM5k9Yc/r/jwov/777/R8Fwhc8+QhSCFGHDGAqHgokEgwdkPEgxKJiwYZAaBliAE8IA4S8w8jilG3YAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAC0lEQVR42mNgAAIAAAUAAen63NgAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAhUlEQVR42mOYvGTrf2IwA4jYf/7+/4MXH/w/fPnh/yOXH/0/evkxmD508eH/vWfvIRSCFB1BV3Tp4f995+7/33XqDkJhaedcvBiu8Pzz/2AMEnz3/T8Yg9gwcRoqJNqNJ28+/3/s+rP/R648/X8A6OvdQN9uO3n3/4YjN/+vOnAFoZAYDABKiTiG/ymzBQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAuElEQVR42mNgQAJrj975v2L/9f9L9lz5v2jXpf8Ldpz/P3/7uf/ztp79j6yOYdneK/8fvv76/97LT/9vP/vw/8bjt/+vPXzzf87mU6gKF2w/D1dw9eHr/5fvvfx/8e6L/7M3nfzPYB6Y/R+G52498//qg9f/LwElL9x5DsezNp6AKJy8ZOv/wMwGsAAIz9xwHI5hYnCFQVmN/8NyW/5HFbb/jy3u/B9f2gO2Be4umEJsmA4K8WGYOgDDNeQHi/6c/AAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAATklEQVR42mMo7Zz7nxjMACIu338NxpOXbP1/4e5LMH3o4j0wvXbfeewKz91+DlV4H0yv23cBoZAQTizvhSg8//w/GINMePcdkx5WJoIwAGx+I2e58AyyAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAM9JREFUeNpsUDFuAkEM9KH9BwehQQglD4kUpUpPzSvoI9GREqiiECnSIaUDxD3jdDS0eQLrcca70OHV7tprz3i8xXxVmYglA6AKmMXIC4yDSPE4HgkrxM3MHcDPY10Hgp6GPblnu/0hKJTex/qbhNPJW3P+q7a/pH59eeZL4F4sv9hSVTMoen98bn7YPDDQiIu6mJymo0hC1UJkBl6OqFmg+0n/DW0gmPVJuvhE12UdZCisX3Yz+eChxPUfLCQajoumPc3e2+ikpBOvEPsXYABFrsnxJcVVtwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAhUlEQVR42mNgQAOxxV3/sWEGbApX77+KguEKZ2088X/WhhP/uxbu+I9X4cwNx//feft/2Z4LT/bhVThj3VGwwt3nH+/HqdA8MPs/MsbpGZDk5CVbwbhpynKw4v1nbv47eu3Fv+aZGxC+RVYIwiD+vbd//oHwlFUH8SsEKYBhFIXomAELAADX/NObsuyvnAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAAyCAYAAADvNNM8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH2AIUEAIR06JGSgAAEzBJREFUaN7NmnmQXNd13n/3vq27p7fZMABICgAXgIBIkZQogaIlUSWq7JAWZHopWeWKy1Ys24nLdijGSVWSSuI4+Sd2QqOSeK9yJMuJ7djFyC6bJZGiSEqRuEQEQRAQCYLgYB3M2tPT29vuvSd/9BuoAQ5IgCZlv6lbr+f16+773e+c7yz3Kb7Hx+5b34tzzlNKVcMwnCiXy1NRqTKJUjuMczuz3FxvrLvBOpnBuQXE/RU2/W8odfrFZ59+W+bgv9Mgb7jpFiXixrTWV4+NVXZOTk7uGh+fuLZarW9zoqY7/cHUIEnH4zSr2jT3MjISk5FmOcbkTU+rz9WiaDMuewBYfjvmpN4psDfechs7tm+78Yadu/7r2Fhtr3VSy4xTrbUu80vLLLfarHW6DOKY3Biss9Y5t4Zzi6LkrFYqc1buNEJjolFL9/Q6X7jp1IJvtULBV4EXgOP7k4X07w3TpVJUm5ia+dzCavyxEy++6i2ttOj2B6RZhnUOrZTzfNXyFEd9kYNKOKhD7+R1mWHPUntzOTO3PjvVuP2wF9KLk6gfBD+nPI0Gcuc+7Sk1q+DZ+0szXwYeAdr7k4W/W6b33nX39VG1+TezZxd3rqyuEgbey56nD4Wh93Il0Ie3dQezN59Zzn1jZ6yndyFyhxLZK6htSis/1JqkWuYv6zUWrbCpUePfmYzduePEIOZMHLOcpKTWYp20lFJPKPg/wCHg2P5kIf6eM50aY1ySDaIwxPM8ZkreH911evEv6oP0DlHqg07xMw7elSu2iJN66GtdDwKmo5DpKGI8DKj4Pu/1NJ+3hkNZznNjZW6RlD2NGjurY6wZw3ySMNsbTCyn6Y9k1t3jKXWqsIBHgb8GVi+2gLed6ftLMwEwkfne9d/Zff2/b1Vqd59eWOS6pJ99aH7FU0p5odaM+R7NMGAqitgURTTCgLLWKKUQEXCCdkKUOx4Vx3/Gcq/4/KtuTlbyiMs+eaBAa1DQM5bTg5jTgwFLSUpsHSLSUfAQ8Iv7k4X+2wb6/tIMwBiwG7gV+ACwU5y7en6yedWRG64vtTs9/N4avxCn3OD7NIMhi5HWaKVAgXKCb4Qwt0SpI8osQS54ToiBExpmBKatIIDTijTUDMoe/bKPCTzQYJ3QMYa5JOZIp0M3MTnwyf3JwpffEuiCxS3AZuA64DYR+YDATUqpCU8pFXmaMd9nPArZHEV8c7zO39TqtBaX+A9RwIctiBM86wiMI8ocpdQS5g7fCEoEUMgbzczJcOKeBicY5ej7wmJZaIWWjrYkyrBmhaMtJ9a4LwL/ZH+yMLgin76/NDMJPADc7UQ2O5jRSpUqgU8zDNlUipiMApp+QMXzCLQm1IolBQ8HPrUo4ppuwmTiCDKHbxyeAyWCEs6DFLUB2sLclYAqhUijQj5RpWti2q/O0rMxaWrIew6jBC/0kGaFKPQoh0p1c25Tim3AS1cqZPcJ/IvpauBP+WVmwpDJUkjNDwi1AgERQTnBM0KYG8qZ5Q6Bzw6Eve2Y23sZsgGLF/zvHKBQvgdRiCsHmGpEVvLphUI37dNdXSSePYbt9VHF3/qhBcLUUu5nmKjCRBk6A3Yp+MiVgxbei4//0Y9cy/SNd5A9/woyexbPCkHqiFJLKbMEucO3gmcFgIbAzm6OBuxGLDoBa1FKocYqMN3ATFaJyx6duEu3vcqgu0S62MP2Y1xuQASFQit9yemGSY5yQiNQaE+HWHcv8HuXDfr+0ox2wgdqoWP7Pfcw8xO/QP/YEbzHnkL/94dQ51ZAqw1Nc51FW5goClQQoCoRUi1j6hWyZomBL3SzAd3VZXpHj2Daa+hShF+vo8MQVSmhRZBujNicoedf2vG93BEkOVQC6iXFWp+P31+a2bE/WZi9XKangR2Niqa+62Z8P6C+YyfyqWlsvYH59S8gZ5c3ZtFYUArdGENtmcJeNUFc8ekmfbqrK/SWT5GeWyPv9HFxisNQvWEX7/r5n6d5xx0E4+OoMARnsd0e8clTtJ95mtZjTzA48RpKBaiNLEhBlOTk5YBmpOjEqoyTjwN/cLmgb1FKxra+axPlHTuRuI/YDKIIb9/dqE2TZJ/5VYhTVDRkRcaruIka2USVZMyjG/fozM/Re+ElsqUVxOQoNEp7hUUIY3t2sf2Bz9Hcu5eVx5/g1O/8Lsmp06AVlWuvZXrfJ5j8/ru56rOfwcYxc5//Isd/9dfIV1YvYeIGbR1V38P3lDJObrsSn75RK/G37LoeXakiWQLOgLXgDGrrFMHdt+OcI2mW6Sd91hYW6C2dJjm1QrbawfVTROzQKJVG6WBEnC3197+PPb/9W+TLKxz6qZ+md+gwYsz5e/pHXmLlkUepv/92dv3Gf6J+2624eIBLkjeXIxFERIDelYDeHoTa66206SwsUG9UwRoQB2LJ0gHH0nnWDh8lOTOPzRM0GqU8GDE9pbwN9NHR2Hs7N//RF+gfPcrhf/Sz5Mut4cJcdL9klvb//Razv/4bTHz4w7zyL/81GLnwNwqQFkUa+JggIM0cbqir564EdF/Q4i2fVEf+9M+4/ad/gkDJMLw4QzDVwIhiMHsSrQI8HV5+mVevsuOf/wp+o86JB3+TfGUI+JIVkvLoHDjI6pPfOA9YMYz3RoRYe5jxGtHUGNFYiBdqWmfWEAfA4uWCVg5WrMVpnG6+/FVOfW0z137kTpSziLXgLNf+45+ie+g7ZGfOXXai5yRn6w//MJvu+yHmvvBFVh//Okq/+ZSyk2cQwKAwnocZi5BKCb9eoh75RP0BbrFNO7d0JsqkRlPkP2cuBVqNnBWgLLLmibgkB7+7TOWZv6azeze1evm8b1dmmkx8+IPM/8lDlx34vVKZmU/9GAArjz0GyBvWwEqEXIS+9sgbY0Sb6lTLAWGSQjfGzS0jcc7ADGm1jTLK0+SuyOpgaRS0Ghm6GF4x/LazgymlJM4F52DsyHPoJ75E56P3UQ8diEEDM/d8jIW/ehjpp5cBWajetIfae24mW16me/AQSvmv880cRe4P2bTVMmG9TCPQVNIUO98i7cSkTlBaj/oACDhveC0brkF/1KcvBhkU18NilM5JpifFlzgfTscoTf3Jh+ntuInu1quohRZxjur2zUSbZohfO/GGfgmgBMZu2IlfrZEuLmLi3jArEyET6GpNNl6jtKlGtRzQzAy0+8jcMraf0Tf2PEClN3Yn5w2v58Nb20CXEXARUBk5l4Dy+vmEyxp7vApxJogC63nQWqL25f9J+ycfYC3NqAeGsFaicfttDF47jkK/iVIoxrZvR0URvdYqXSv06xVstUJYLzNZ9qgMEtxim3i1T+JkmHaqETbfpCdkPY1DkTsHML8/WTDroMtADZgo6uJqAbayviAtZ2pWJE8MJQCrNCiNvPwi9ZMvsnzVu/HjAdWgS3XXdShRl9Sydd9MNZyaPc2ZB3+T9MRRgqubzNQ91FofObeE7Wf0cjtMN5VC642rLxE3TG0vDocKRCssYIe3LI62i6pFmrkVaBTA103bA3RXnG9E0iSXmhMwWg1X2jnUV77E9Ge2MBcHSNaium0rOiojeX4epBPIgNT3SQvfLDUrTNZydn/kPVR/9id57oc+TevQi2jtX8CmulgJRPDGyviT45S2bKG8/Vo6zz1PfPz4BVHDaYV4CuPAWkEJp0ZBhwXQBtAsmPcLwOvqrQykWS7DomjdX7WGudOooy8ytesWzi1Z6qUSQa2CWVklFsWq8skaFSpbGjSrEVNZhlrtI+cWmahtpfq+j6HDgMqe3bS+9dSlo50IXrPK1Z/9LBMfvYtwaopwy2aSs2c5cO8nNih0hgWQdUPhVmooYoyImF+YdKnw63BE2DSgMlwvzYVcFG7U1NIUeeobRIMVtjSE2dnTnKmUOH31DIObt7P1/du46Zoa21dWCQ8cJ3nhJOnpFdLWgO7Rl7GDPijFpn2fQAfBJQH7jQbbfumXCTdNc/r3/wA8TenqqzCtFvnq6utyA+dpRCkyByJigLOjTMvIWA9Zr1vvTNzAWCG3Q1UcVrRFCnj6FDJ/jmhmmu2bNPm7JuHsAjK3iOlndFIz4pv6vKn2XzmG6XQJxsepv++9jN20h97Bw69XfgUuSzn1O7+NabVRpYhtv/yLw7fCcKN4eJ6Y3IESUtR3d0c0kANpcbaA2yhLiHFdY4XUCE5p3KiKZhk8/xwS96k1m9DOiU8ska7G2GxYWl5c/ik08asnaD3x5LAi2ryZ7Q98Dq9e2VD+XJJiVtaGeVXusP3BsG6uVDaMFNYfXkvtsFkKtEZBZ0ACxMXrDUEnyEDc8EtEgRsF4Xlw+hSsraLE4IXhxjXu6xhxLPzZn2MHMUprNu3bx8yP/hjO5W/8MZtjBr0hgCB8nXIrBKc1AuTDSqO7XmGtg06Ki2vFGxsCH4gbOCcuzmUoEEpf6AOdDpw8ibIZOgwub3tFebS+8XXO/o/PI3mOX6+xe/+DbP9n9+M3q4jYYTkq7vxrVQqY2ncv5Wu2ce6P/xcv/dI/HU61SK5N4NFrlknHAoShOypYBTqjPp0VYNeKeF0rsjLvIvOORcQmOVpUIWZ21J4snJ1DvSdF+95lV1lukHHywf3Ub7mF5ofuxKuOcd2//TeMf9+HmPviH9P59gFMt4ffqNG88w6mf/BevFKZk//lQZYffRTX7SNeQFbyScZC8tDD+cM9r8xCbgUU/SINvQD0gOFqVArQ/oh6K4BYXOIg62USCCNha/Q4Nw+DHiq4/H6jUor4xCkOfurT3PAff43pfT9IOD3Npvs+yab7PokYg40Tsvl52k8/w9k//Dytx59AlMaUQtLJGmklHKacMhSubiysptBOHDLcK/jqqE/7hXilI2x3R1LR80qeixgnkqdmGEJi36eWpRcm+f0BamwSXSpf2X6x0mQLS7z8wK9w7n//OVMf/ziV664F7ZHNzbH27edYO/gc8XdeIbeGrFYhLYfkkQeFSqcGlhNhLRXiYT6RA48CDyl4aH+ykF8cskxBf7tgvHwRaNXDxbmW7vyKaa7MhKhyCd85JpKRzcE8Q6caXa2fzwiuBLjrxaw++jVWHnkEYbiLIQB+gIkikkaJrOQjWoMIqYVuIizHQi8XjJO+VZyIxT3cFvMXX8razxfY5OJdSymEKyuAdwoTr48kKWrFme5rLj1Y7Xlbn5nNvR/YHbBaLlO2hko+bMmiNHL8paIf/Ra3yZRGEwKCCT3SckhaCYYhSCtEoJ8JrcTRTiG1gnH0e9gD51z+yJzLDrxkkyMGWb1Uge5/N5xjCt9uF+bdKLIzH1AGcU/m3QM1vKZb4IPfKmn/zh0e85UqW/pdSsaA78HpV9Fm61veELS+JisFJJXCfJVCnJBYoT0QWokwyEWMSDfBvbIi5v992wy+csZlcwVhvcJFk0uF31HQtkhQ1tnujpSb5zO1p03v2UboTR+bU7smq0q9e0azVB7jql4HjYa1FlpNXLZpKxmGwDzySSohWcnH+nrYB3DQTR3LidDLZKjGIqurYr5xTvKnXrHpi+dcNu+Gcx0UIyuGfTOm2cC3VwrA66ADgGUxvUeyzsOfpNl46pV8Sy0KuaYZsFgZY9OghzY5uqTfPC9RYAOPtByQVkJMoM9vZcWZsJoKrVhILM4gywnu2FmXPXbAxk8tuXyxmOegYHQ9ozQFu24E05uCHvXtdiFo61JcWe+0LEneOWD7X9tLdd+zr+X1xp4QFUWE1jKBoAO/aNnI61h1WpGV183Xx2k1rFIttFNHK4FeLuQOSUXmWmK+fspl35x16dGFC8GmxVzNCFh5w0bbJR6/cCNsj+7TpIWPrzcWvINmcCxCf8Vbq+x7/Kgq/cC7A1rlCmEQDjMy9d2fF63IR0TJeRqFYCz0U2E1HjKbOoxB5ge4w6ds+vgzpv/MALeeKQ6KVDkfqRPkovGWny5yxZf2RpqF635SL5oOkUB4wPRf2qKDa7yW2nvwjFZ3bPdYiSLyYAjaeZq0HJBUguGTAkXPyuRuaL4JDIxgBBuLO74k5rEzLvv2yzY52hXbLhY/KcY6WHclrF4JaClW1RRgO8VoFm2lGlDNkOhreeexfxA0osMn1S0lH33rTsOq72hPjJFHPqKHZm6s0Bs4VmJhLYNcSHPlznSdff6oSx85ZAZHMmStABqPALUbVH/C3+LwL9mhvdB81ld53cQawDhQWxNrv2l6X71HNba+cJKZzdM5pSlDXgnBOdLU0UocqxnEBixkfbGHFsU8dtKkzx9z6WwsrnMJ9XVvxXz/ts+RjfbCg0LJqwXTzWKMA/WbvfKN3+fX/uHkmD9545YKrcUeK7HQNUKOGmTiXmuLffawjR/5jo1fHfHTdfVdFyT7TgB9K49UqZHeeFikqNXCxyeAZoiavCuo3bXbK/94qCS0oshF+l3c0/Mu//qsS184btNTFumOgM1GgLp3GuxbfaRqdAdkvV++nrKOA9V7gsbHZrS/uy32yEETP3nSpXOycUy1lxNT/z6AvngLaL2bWik6qpXCDVwBLhkBeilWv+eHehs+u95NDYqhR7I7806o798l6I3ETo18p3wv/fRKjv8P+ck1e2My4ekAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage8 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAANCAYAAACZ3F9/AAACn0lEQVR42m1SeUgUcRj9lce6ujs7O+PM7LibR3jsoSu5HltS3oIH5Fr9sRkFBVJEBymUFIlkoqEQXZZISqhdmpCRuovUaiomHiAmmUYUWqCgRRiI+pqsQKgP3h/f473vgEfIhsrIyJCl7bHb02z2kvjEzDpL5I5RvSl2/GBA+OFSSms95cMLkmzTRg85QYJlef5h8VFxyRN2nXH4gk4/1qAzTV7zN3+r2hKx2qkxzjT6hT2toANPrw8oYgP1VwIMN+o1oYNtOtOnEn3c8ks/8/JHLgJz6vB1fBDMmGJD0EOJuMsK34vV4gFSm5xaOHG1am3BdgiLfhbMC5GY4YyYFvR4G5eM0fgkDCakoNdsgdNDgVYFjcuM8ITU7Eqsnm1/jMW6GgzxQeijRbxQsnhljMSIbR+6g0LRb7GiP2Y7OuQqtHEcyhnRRSoCtXcmj+fia0stBizRcLh5w+Hug6mycjhpDh1eSrSrGHQwvnBK21qVNMrUYhPJ55mWN1YRi1UFGM7JwnMfGh00i+nqW+j05dEpCbsDgjGwMwldFIdHUn+J0VSSHLWqdSSKX1uyR2N8fzYc0tROuRLvK6swlL0bTk8FeoINmDx3Hi7/YDRSDEoZ8QxJUiiaercJKz9StHiXGi0Jlevndm8Nw2xDI0Zy96IvLh69qelo5sW166zwuVitySR6mdftewZxfiFBxJTVAKeM+v2nBFdYOFzp6XhgNK5V0vyXsyru/kk5b8snahVxJ6Qg3VvZ3BWjW5qICoLDm8YzOYWHNLN6k+PnLqq410d82KI0D1V0LFGyUlY2/w1NlCcheUd5X2drRMhKHadZrmCFsUKKq8/1pG2hRK6VNJ7/RO0PKRhk3lnHWE11vtK3INuDijURipF4t/8ZftVPn7EVIRn8xrEAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage9 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJEAAAA3CAYAAAAMnajQAAAR70lEQVR42u2dh3NcxR3H+TfIQEKAJGSSUAaS0KupNgYciukQbEwHY3o1BhyMAdu44a7e+6m3Uzn1cjrpdGqnrtOpnHq3LP/y+/6u+O6QVWwJBs/uzJs7vbe3b9/bz35/Zdfj84hIy0ezOtRxhof2PMcXVVQ509KsIFJFQaSKgkgVBZEqqiiIVFEQqfKbgOjE8eM01NZEgy0NNNjaSNOT43Ty5Mll7RXaH5+YoMnJSTVE5wJEA031lLrxCYp+4hZKWH8/WcvzfxGIzM0t1NLWroboXICov8FEUY9eT343/55C7vs7dRRl/SIQ1TaYBSRVzgklqqOEDasp7IGrKebJW6mzTPcLQdSoIDpXIJqenKCe6nLqMhRTd1UZTY2NKIhUWRxEGFDvY7mLguhci86mJmmguY76zSbqb6yl4+NjNDI6RtPT0646U6PD1GuqoMaUSDKFHSZzUjirl17OzwrJzAka6WyjjkIt1Ub7UU2kD7XlpkgUOMPXnBA1mBv53vX2e/Mx3tcjR4+xnCwluWSrreT+jM77lCdmZsjWPyCfiynT0yeoq7uHTpw4sWRvfIafrd1ipcaWVv7sXJJJiTampqaor6//jNrD89U3NtPg0PDy+USpb62l6Mdvovh1KyU6g9Pbx4OCDk8M9lHp/q8legu66zIKuP0S+YxeeyOV7NlCk4P9eMpTD8wd7ijIpLR3nqbQ1VdR4IpLKeCOSyl05eWU8sZj1JqTLCABosqiQkp7+ym+981y/8Idn1D25tco4j//ptD7r6Bo9tEq/fdIGmKugpeTkZNHQ8PD3PaM9BsvzvnCnX/j2nGeHHXmJhobH6eunl5KycimXls/NbW0Uf/goNR1tuH+HUDgncw4QMWn+z2c96msrqHwmHjS6vKpylR72vZm6+epdmdc7c046ra2d5CusITGxyc8nsf93s423c9hYo1PTFJsUio1tbadtu9nH509doM9Oltpj84Mxmrq7evjwZuiiqM/MASXkD9fRx2/my60f/Lhy0fFsR00M31qkHtNekp6dY3U87/lD/b6+O74beJLD9BQe6NAZCjKF3ikHl8P5ujQ54bfcb0LXOfC1/xLfLb5INKkpNPAwCBV19aT3mCkrLxChqXRBUA2/11mqKJKUw19s2uvvFRrVzcVlJRJqmHXgaMUGq2hjk4r1XHfJvjFT05O8Qxuom6G7ZBfEAWER1EzD8TE5CTlFZVIm1Ac52BgYDJz8ygjO88FwBRPgMbmVlEStAkTDqWvqWugwtJy0ubmk7nJbtZxPr+4lHILisnGqjPKfwP4wlI9dbC6mfg3uIfF2kU5BUWUW1jME2dE2gZkRWV6mQjOMszX0EdtbgEFRkQLRLAwqJeTX0Sd3M4CQDo7iIYtLaR5/h655n/LRZT48oOk/eRFe30GwpcPzbr7GIomO+HcwbL9W8mP6+I3oasup8wPX6DUjY+T/61/lHOBd/6ZGhJCZ4UICpT71ZuU+dELrHgXyzmf686nutgANpEz80KEF7/3iK/AkJSupf3H/OWFa5LTKFWbI1ABsq079lBimlYk/qBvENXUNwhE0fHJ1MxAhbGS9PTaRHkiNYnUZrFwvUAKjdHIQCRy24AwPVsnYPXY+lxKg4EOjoylAgYEvx/gQUVdDG53by/FJKZQW4eFdu4/LG2nZGbT0cAwAT1LV0CpmTnc9yyKiEukhqZmeZ741Awy1tRSHD8HfhscFUfavAJpC23j/seCwuTvkVG7+UcyNzohhe+RJO/ih32HBda0rFyKTUwVeBNSM2WiLCtE7WyWgu76iwOIK6g9L52mRoaoIT5E/gYQMU/fTt2VxfaO9/WyaXzcrlI3XkD5296jsd4u6mN/J/bZFS4Fy9/+/s8hYiCLd33G7Q9TF7cXvuafdoiuP58q/XZ7qN1cEB32D6EqfuGQ8B08UCYGJClDS1HxSTJbBwaH6CefAIEL5gygWdkvCuGBqak30/DIqAxStwMimCaYSbRvYFPVyeq157CP3AsKEMntVlRVe5jO5tZ2GaDAiBi5D+AcEIhsFKVJFhAAJa4dZ6UKj01g9dLR7kPHZIAB6ZGAEDGNmBCjY2OsZi0MSaqoj09IOE2zIvU64IHShUTHyd/OYmFFPcD3wDPABOKZjLV18uyALYHBPOQfLOO8rBC1ZCWSv0MRQu+/kqz6AnlJoz2dVBW4j2qifMlSnG13sHG+20KJG1a7IKoOPUgz/LC4nvjKQy5zqP14/awQGdh0ChStZop61N4nXzZvFUd3zOkXCUQ8SwUifvlQGAwO1KWBvw85ZD0o0j6oHhAddUDEgwVFGmGIglhJAAtmOL4LRNy+wWiS82gX72fKAYDBWOMBEY6+gQGe/YeonE1rRFyC9A2/RX2BiM0jzCT6GRYbLxBh0EsrKkUxWts6xE+DCsGPA0TRPPgwYz7B4WKWernNI4GhYlKhSHhOd4igYlBAgYifA5PLNyTCdQ/A7h5ALQtE8EXCHrpGrgXcdjErxWYa67Ha7T3f3MOeAqKuDkp48X774LNfAwWBOg1zpJbkBlHmR+sWB9Gx+SFKzsiSgQoIj2Yz0CKDg4EysfmCWdCxD4PBAjDwD9KycsS/ORoYIoMZz0qDgUAbIVEaUQA464AS7cPHEj+Kfw9VS0zLFOcZCmBzRE3wV2Du4OvAL8LA4Vp8SoaYFJhUKAcUcduP+8WUZfI9oILwz2BiUUfPygbfDL4a6gAi+FIACucANkwfrsUyWLgH2neHCOYMfUvgfgI8KC4mF+prktOpvNIoUeTM/BHt2UEE05W75Q2Xz4LILOeL18lWV/lzH8ULIkABUwd/KOXNxyjo7suWDSLMJrx0mBcAA78ALwfmCQDAOS7WV4jfgfP4LOPZCLVoaGoSRemx2WSGwtxhUIrLDeLfYJYDyH522kv1lZISwGBBldBGV0+PhwoBsuLyCtLzIMGvknMMCM6hb1AhmLUjAcHi4JboDaJQqAdHGCYTdeFII4KEoqDPMGl4RqQlUL+E+wI/aXhkhPs3Lc/krSp4FvTRWFMnqoY20Cb8wqKyCmrlvswst2MNUDpLdRT/wn0uf8bv5j8IFG26NDZV0x7hpAdEclzois5cER0g+nBpIXLe392czJVIPV1ydbb6rt/wu3DmuLzvc7oELjkO7/YALxxyp4LN1S/v/nnX877mNa/nfc5lD/GdpU2XSsmvPyJ+jlNNkDeCT3SCHV50BrNziM2WO0RRa2+k+PUrPQ+O5soObFtSiDDAwx3NNNhSLwcSlkudeUeytbuqVHy8hSYI4TtiYnn3BX9DVbzPw0+DWpyu69ImX0c9mOAF+DO/4Cr+PBBhkGy1BkkEInHohCTy4WupRZsgco8EpdVc5+ETGXx3sbPdKRGa+zEx0LekEI3ZukjLJjJxwwOU/NrDZIo4StNLvFcJkaPuq410fGxkQfWRaTf4/kgGn50LyrqjwD9qcss7/axNfgfJGdlsCkPFQYYfNN+eLLSFqO0ssvJLA5ETJABRfmg71/uHC5SUt9ZSf4tZ/AdviDCYmLn47VB7syxxYIll2NK6tBCxs5/w4ipROEtxjmywwz16jGVkLctjk5xL0xPj0o++eqOkKrC00m+ukQBhkPvfkpUg9QGfra6KemvYhyrUyjn8ruC7DySqRMYdESkWrieHB8lSlE0t2UkSPOAeXYYiaRuf5pRIas6IE9/Sqi+k5sx46muo9nBm4TTDn4F/AicYUZgkFFmp4EzDF3MtP/E7iE5IpkpTrfhbSCG0cBSH83DWEW3B50H78AXhO+GAI42kJSCEv4hcGO4JX2rZk43IWEM5OkvzOFKrkBvixemPfE+BDkUKWXU5NWUlMUT1p4UIIX7WZy9T2IPX8HE1FWz/cOkhWr+KdFvfplo2sT3VZVQTcYwi+bmQ7Ix77k6q1wQLTKnsz6W/9yyfu4uKdnxK3ZUlkkDFkf7uMwJJ/rfvy/UMrqd5/m6yludRwfcfUgQrr/bjdRT7zB1i4i3FuQzXR9JezpbXGTyD+IvxL9xLJfu+opwv36TCnZ/IPUr3fc0BxXpKf+dpWaN0lryiUkk6Ig/1/d6DVMeKjugOkSByRojcYL5OQZQiAQOc5iCGCCkMRIv2I1mSizB1geExFBGbKOr2xfadEskhGkUd5IlQD5HgAhTqzCGyWjrIGHKAYp66jYLv/Rtlf/6qzDQZ5LZGebn4DZKR1VF+7PHXCUTueSJj8AEXRO4hPgZsPogiHRvlABFMwnwQaf57D2l48NI2PU0NiRFkDPpJ/DYoCe6X+9VbsgaYtukpUQNEjUiGIt+V9MoaMieGUcobj8pgI2ue9vaTomKAqSbKh8F6TyDDb7H/yhh0gP2wFjKFHSHd1xulvx0FWop7dgVPkvdFeQG17ptNkj9Dlr74x80yiaBO0m9WDaQBEDlBPQATEpf+oZGiHPZzSZIZd0KEpCHMmX9YlJgzRJ5IfiJKGxwaon1H/CTiw3UoE5ZokNoAWIgoA/g8TCCiSkCI5OqyQdTT2yMQyVqWY0kCZgAyjhmHlyvrXXf/lWo1QQJRd2uzR8Zat3UjjffbaIihc9b3Y2Cw0Drbskfp3i9lVwEUI3T1la6MtTH4JzE7c0EEeOvjQ2hyaID9llEyhR6WtmFmAADSCoU/fCyf2E2QtulJgUjP5jnmydvYTH9L5Qe/FbMG4LI+fUnMHcwkJgPayNn8Ko3bumVtEOqI3wA8PcOPRWRk+DG5ADBMGCDK5XcA0NA//eHtrGIryJwc4QrBkTeydttDfOSqyg1VAgDMEApMHA4nRFCSgpJy8XPgWNczbMh0Y10OUGIZB9fD4xLE5E1OTckyDtIKSB1A2WDGYCb92K9CRntZzRl8A+x2dF87K9r5KUvyM661LYGrJFd8ooH+firZvUVAkSw3g6Dbuomy2ZQFrviTXbkYOnNy5KwQYSBgYjLef478b73IBSO2oMxlu50+EUwXlKSJZ3pV4H5RIoFo+3uU9ckGMidFSLoi73+bRDnyt73LfoqGEl56gFXic6rmSQOlgbLA/ApEUJ2Qg1TAJs4JEZx3mHRMBtwXv0V77fkZoojVDB0gyuNnx0RCXZjUIq4f+ci11JAUbs9vsUrHp6bLelZFlUmWIbDyD1Bik9Ikv+PHqoTckcucsSlCMtX5PpAmQFI1v6RMzCCy2VA2qBrAsWfV4yXHhfMBYdGkr6yWZCXM5fF5UieLgMhzj7XLJ2KKEWE4188EJsfgutbB+OWOsN8EiJAs66oolBfp68gNARAnJDBZyTxz7Y61mQyFnhCFrLzcBaezPlICUI65CrajIDtesP0DObDfqTU3hQzHdkq4j5kPnwgqh2uIGu2D+qlEijgHIMoPbhOIYHrq4gJpxNpOVQH75J00pkZLG1A67KnCdherPp+Kd3/BkHwn90caAFtXsI8K+7Ia2ESibTjpelYtKBpUCtttnAVZY/hDMFnYUQCzA7OUxv4KzsM0OR1xKI++yuhKTrpv8MNaHbLo+D42Ni5ZbzjXuI4kKvwfe5LSIPAgw29ZslV8x2wLW32V7N9B5OFKNmIrA88ozKTIR66TjDWWP/AJ4DCTR6wdIo9OiBDJYKZhbxLSAajvj9/wd5g6zFb51x48m4wlRR4+kY4d0Zwtb0h2G4u7kotiB/nkEm0a62+sYVX6QFQGe52wYe6kI4F40msvzgKTQQtO2i0kOemdEJxZRJ8W0/4id7AubI81zBbWyeDrTI0i9W7yCPExqzp59ph4QDHTqsOPiP3HrERnEKY6IbJvCTkuClcfF8QzeS9V8eyDGmDLiHOwsChoa236mWMNVQHIzRka2dkoDvUS7Q6U/BSDYwzaL+H2QvM3anvsGfwLWG+IXDMDu+kYECx3uJPsDZFrls446juy2t5ltMsya3S2nAV9P11/VFlmiObMzs4G0QLKrwGRKgoiVX4rENVw5GTjcH35Ieqg2GfvlPAfjnclR4KqnCMQYX1lMSvEZwoRkoLm5Cj5J0U4sMlflXMEosWWM4VIFQWRgkhBpCBSRUGkioJIFQWRgkhBtBwQDSmIFERnAxH2Ay9gl5wqCqLZi6yQT0ws5F9TqqIgUkVBpIoqCiJVFESqKIhUURCpooqCSJVfDyItqf9RWR1n8b9R/x9tDcyxLI7LzAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage10 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QAWRXhpZgAATU0AKgAAAAgAAAAAAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCAEiAZADASIAAhEBAxEB/8QAHQAAAAcBAQEAAAAAAAAAAAAAAgMEBQYHCAEACf/EAFAQAAEDAgQCBgUIBgYIBQUAAAEAAgMEEQUGEiExQQcTIlFhcQgUMoGRIzNCUnKhscEVFmKCstEkNEOSovAXJTVEc3TC4SY2RWODJzdTZIT/xAAaAQACAwEBAAAAAAAAAAAAAAAAAgEDBAUG/8QAKxEAAgIBBAICAgICAgMAAAAAAAECEQMEEiExE0EyUQUiM2EUQlKBI2Jx/9oADAMBAAIRAxEAPwCppMvYkODWuH7JSeXB8QZ7VK/buCsUMB+iELQuf40dDylXupZ2jtQvb4FpRBjc32mn4K1XRgjcX80VJQ0sgs+njd5tU+J+iPKVc5o8kEtCsmXAsLkvqpGg/s7JJLlbDn30dbGe8Ouq3j5H81EAczdeBkY4Pa5wPfexCmk2UIfoVUg/duksmUJvoVbfMsU+KRLyRG/DM5ZtwstOH5lximDfoxVrwPhw+5SnDOnDpQw8t0Zsq6hreDaiOOQHzu26jkuU8QHsSQPPgbJLLlzFY+NLq+ybo2yQl45ei1cL9JzpCpS01lNguIb7h9M6MkebSpRhvpXT6QMUyc15vuaatLLe5zXXWdZ8KxCM70c1vBt0lfTzNd24pG/aYQpTn9i7cRq+P0iuizGAG49lOtDuZmoYKho95N/uRgzP6MWYDqq6PCaR549dQS01vMtACyK5pHcg78rjw/krFlkhHiT6ZsODo69HvMDjLhOOULCRwpcaDQPc4ouf0Z8p17DNg+bMRDb9kkRTsHvFj96x8/fdzb/aF/xR9PXV1I4PpayqgLfZMcrmW+B/km8t9ojZL7NN4j6LuMRlxw/NlBMOQqKV8f3guUcxD0dOkak1eq/oeu7uqq9O374CqjC+k3pEwwj1POWNxtAsG+tvc34OJUqwz0ieleha1rsegqwOVTSRu+8AFN5Y/RG2QpxPoe6TqE/KZQrZgOcDmS/wlRjEctZow9xbXZZxinI466KTb3gKxsM9KzO0FhX4HgVY23FokicT7iVJsM9LenADcTyZLc8TSV4I+DghZkRtl7RnqWZ0L+rla+J/1Xggj4rjMQe09iQ37w6y1HT+kr0VYs3qsawDEYgR2hNh8U7fiD+SPGcvRizADHUU+XYXO49bhxgPvcAPxTrNERxf0Zpoc4Y9RbUuNYjEz6ral1j7r2Tj+v2NOI9bmpK3kPWaSOT77XWh/wDR96OOYf8AZmJ4ZC48BSYwWn4PcUmqfRlyBiDNWCZqxOIG+kiaKoF/gEPYwKBbnGgmv63gGGPB4ugL4XH4Ot9yMGLZZqLXo8RpfsVDZB7tTfzU76RegDCso0Dq6r6UMMpImjYV1ORI48gAxxJPkFn6Wfq5ntjf1gDiA6xGod9juPIqaiQ2yyTHgs4/o+LSx35T04+/SUA4WXC8GIUE3lIWH/EFXTa6RpHEHv5JW2uro2a3RzAHgdLrfGynZ/Yu9k1fhWI3u2n6wd8bg78EkmgqYzaSnlHm0qNxY9URn56xH1rJwgzVWNaPlSRyF9k22X2G5C3UL7mx7igE3vbdcbmgv+eihk84x+KF+lcNm+co423+q4hFMmwBcEBztuIRzp8NcOy6WPyeCgE0f0Kq32h/JRTItBTtVkAuf4IT+rv2ZmFJ3zMa6xkZ8UUybH3JTv8Axjg3/OxfxLVT+BWT8kSMdnHBrOB/p0X8S1ged+9UZVyWJ2FOQHNSjSFwsVaRIl0IJYlJZuuFicBIY9kHQlmhAMZBRRFiVzEAsSsxoJiUpMLQjLEAxnuS0x28UEs2VhWITGe5BMZvwS1zEAsQBUbW7oehHNZuhaFUXMT6EIMJ2a25R2hCa0hwcNiCCE+LmaTEk+CR0uQsWqMJNcXRsZa4BafxUSkidG90bxpc1xaQtEZArBj+Q2wOdqcy7HjnfvVJZooXUWN1ELm2s88e5dDUYI+PdFcoyYczlkcWMZZtayA5hSwsQCzisHqzauRJo/zZccwW4JTpXnM2VfsVyoSOjFkS+FjhZzQR4hLnM3sgPZtsmUCNzGuTDqOT2qWE/uBI5cvYVIbmkaD4FPujZB02KbaidzRFpsq4a49lsrD4Oukc2ToD81VSDzapg9l+IuiizfgEbUG9kGmyfUNJ6urjPgWEJHLlXE2ez1LvJ6sQsN7It7LckPGid7Kzmy/isY7VI5/2DdI5cOro/nKSZvuP5K1HM7KLLSl8RG9lTSwysPajkFu9pCKdfSQbj32VtSQtcO0xp8wEllw+kkHapYXfuBR4gWX7KsLbj6wR1PWV1K0+r1lTD/w5nNt/dIVgy4Dhkt9VHG3yFkimyxhbvZbIzydspWIN6ZCZ66vmk6yasqJZLW1ySFxt5lBFZVN2L2uH7TAVLpcqUx9iaYeYukkuU3C/V1YP2mI20TwN2A5kmwifrm4ThNa69/6XTa7feFM6bpec2PRV5KwWUW9qAviPlvqCiUmWa4Ehs0TvDgk0uW8SZwha7ns4FFtBtRO5ekXJlb2cQyXUQ8L9TOyT+ICyLOJdEtebPjxTDTxJdS9Zbz0FV9Lg+IxizqWQDuAuEmkoqhmz6eTjfdhNk29keNFjOwXo1qXf0POjKYnh18UkX4g2QHZFoagasLzfhNVfherYCfiQq2fC5t+yW38EEw6t3Wd423Qsj9iuBYsvRzm5oLqeBtTH9djg4H4EpnrcvZoor9fhsm3Gw2+KisZniIdDNIwjgWPIt5WTnS5lzLSACDMGKMaODfWnkfAmyZZECxs5U/paIfKUVQP3EgfVTXu+ORvm2yfm56zZwkxQVDfqzwRuv/hv96Pbnqsc3TW4DgdXtzpjGf8AC5T5Yk7GFdG1Y53SBgDdYI/SEW1/2ltvTcLG2Ssap67pBwBrMDo6RzsRhsYnONu0OGpbNbvbmqpyT6JjFoDo2QS1KLLxalJboTFiCW+CVliDoU0RuQlLFxzEpc1cLE6QrYkcxBc1KyxAczipIEhbdcLEpLCguaboASuZsgOYlZYglm6mgKj0LhYj2s3Qur8FXtLROGbLoZvZH6ELTt4I2kVyT/oPxT1fFpcNc+0dQLsH7QRHTPhXq2LCsayzZN+HHvKi2Xqs4djFJWtJBikDtjbbmrf6TKJmLZVjq4QHFrQ4EdxC7GGanDn3wYcq2ZLRQwavFiUmMgbgbbKH9JuYajAcJayh/rtTfq3EX0AG17c1y5RadGzdY8YhXUlE5ramUMJ5W5d6i1R0jZdjrZKZkj5dJsXgbE+CpetxjFnVEk09XM+R57TnPJJ/7Jv6ySWo1Adp5ue4lVvG30WxcUaRwbMWE4qPkZureR7EnZLvJ3Ap0ew2WdcCraqjm0NxEUzidLmybsd4W/krMyvnhkU8eGYnGWR+yyUbgfHgEic4umO8UZq12TwM2RZb2kqAa4Xa4OBAII4EHgins3KujyjM+6E5Yi9CUuaLoBaFNECZzd+CA9iUuagPaLJiLEj2oOhHuYgluyCGxOW7FFuZslJagPaggSSMQNPJKXtCC8DjbZBKYkLN1wsR5G+yCWqKI5E7mIHVJS4FAdwRSBNiZ0aJfClbtvBFu3tbdQ0kWWxBJTNcd42nxLUmmw+nPtU8ZP2U5nxRb2iyhpMLGiXCKJx/q7R5JPJgdC4fNW/eKfC1FObc7JdiLIyGCTAaU/SePeiH4CzcNqHi/eApK5mxRL42pNqG3sT9H+DGnz3gMvWNOnEYeXHtBbSY0W5FZIyZHbOOC77evQ/xNWu2NH3lNtFk6ONaULSjGs2XS0BG0RuwotQdJR4b8FwgKUhQnQgualGlBc1MAmLUHRulBauFo70AJjGgFiUlqA5qlIBOWoBbulJZsgdWbpgKlDELTsjwy6EGADmkLRMWLk3YjLu5KtAXHx6mEeCGQ2RSfMfq9eKd0TbXsT3rRHRnXszDkT1eV+qSMGM/ksxZxo3Q1Ima3nx8FbXo5491eISYe53YqGkju1BX6PLy4P2U6rHeOxox2kdQ4lUQOZYtcSAfBUH014g+TOHq8RIFJCxo7gTuT961d0s4X6tjRqWjsSi+3es1ZxyDjOZM6YkaFsQa1rXl8ryNRLNmADyKs1i/dTXTI0qeRUirZZaWaVofGRtyPFSzo3yhQ5ix2GllhlFNYmTe2pMpwOajqzQ4hFNHO1+nTfieQvZX10NZOxLA5I8SxUtYxzNTIy67iOSyTyWqRtwYmp8oasa6F8Fma19PLU0wA4NsfvPBVtmrJmKYO8xyzOrILEMla3tDuuFravp2VFPGR8ldu5N2j71W2YqVsrpWSaNFtzx1LJJyVHS8UZx44IH0Q4pLW4TNhdU8yT0bxpLuJid/JTV7UmwLC6GmpoqqkhiD3NLHSNbbUL8E4PbvwWyHRxpxSk0JXMRRbYlLHN5WRTm+CYrbE2ndAkalLm25It48ECiZzNkAt52Shw24IBagBMWoD2pSWoD2oASPaLot7eylT2+CLe3bggBKWoJFuSUFvHZALd+CACH+SKcLjglL2+CBp2QAlLe9FlvFKnt47Ist47IJTEb2lFvSt434Il7fBRQwnPsovSUpLduCAR4JWSmwh3BEvB3Sot8EAs3UUMLcmA/rhgv/AD8P8QWvNPCyyPlFrhm3B/8An4f4wtfAC48lJDdgWjYhd0eN0MN3XSEChWkrulGe5duPBRYBJauEHuRxsue5SAQW7oJHcEc4fFBspoAktvyQHt3Si1+SC5u3BMDE9t1zT4I4jwXreCBbZVOiy6G96UaFzSkLwnQvaEa4adzwQgAW3BBHggUjGbKHr6VztP0Uz9HuJvwrGopGus+CXVsfHgptXwCWnc3bcKuKyM0GOB1rBxsbJFLx5VJFjjvg0afz/TxYtleHEo2hxADrgciFSOF4lTQ5ulopZGxPqWMDS51rlpN2g8jv8Cri6Ma9uN5FdQzO1OibpIvytss59LeFyUeZpI3C2lwcw9xBXQ1ivEpL0ZdDLx5qHrOGT8GqsQo6yZs8M0cxLiLWcL9lTyGMVOGObSy6XBumN3IW5JhrS6TDqGQue5kkIJD97O7rpwwKrlo4uqiZraSA2/JcVNnfuyA5pwXNGI5gpKBlTJ6v1l53x1D7sHgDslfSJlHEMOyY1+HYnWzTPkEZMjtWkefEXUgxesqMUxOV3XinigtcNI1vI7/BRvNedmQ4bV4bUYfWTyWHVyCduhvcTbdNdosqKj2NvRy5tNhrsL3JhcSbm93HjZSp7N0y5Mw+nFGK5rnde65cdXEqQFq1Yvijh5du50JizmiizilmlFPZurCsSuZsintKVlvZQHs2QIIyzsovSbpWW7oGjcoASOad0BzTZKnM3RTmbIASvai3tKWPZsinMQAkLTZB0JS9myLDSgBNI0oGnZK3sQNGyAEhYintIS4s2RTmIGiIXtKLcxLHsRehQ2MxIWbIHVFLuqHevdUFVJjJCB0RXOp34Jw6ld6lK8hYkeyrFbNGEm3Cti/jC1o1vaWW8tx6cw4Yf/3Iv41qexupg7Yk0Ac5rGl73BoCasSxiOAOEY3HM8FzMU3V9W0OsONgojXymTUHPvbuVM8tT2mrBpHONi2pzZWQvPykNrcOrLijqTPNK2VsNdHKdfB8cRCjQijc8X0l3HcLslS2OmqIm6Lltx2QbEKzG2x8uBJVRZ9DVU1bE2amlEjTzB4eBCUFpsqZyLmSakzczVMxtPMOqqIdW2rk9vj3q6d7crLQkc+SphelBLd0dyQHDdSQA0oBF/NHckHZAthTm7IGk96PdwXEEFWhq9p33RgbZd07pC6SCHMaRYtXWRtaLBGlu677kELgLMYIIKgmeqHQ8TNG4PGyn4F0z5oo+voz2b+XH4Kqassgx06Acc6nE20cjuxUM07m/aC5095b67Haaqjbs94vYbKpsMzphOTcYifX1WkxzgARDWW78TbktR5lp6bHctUGJwPbLHIxr2SN4HVwK6mK8unp++DHkrHqIyRW8+EtqcrMiaS2SMamEeHJR7Cax8eqOeTRI3ax5KwqKMNi0ObsLghRLOeBPYHVuHOZ11rlh4FcOUWjuxfNje6FmHxzVFPVOp5XXkdIGgtf5ggqqcwYlUY1jsklQ5rntcO0yFsYPlYC/vUi/XDF44Killwtrup7LpC4OaB3nuCjlGyqxHEmvka0Pe4WDODRdEmqQ+TJHaWNlaijpcLYI79oX7Scy3ZeoIPV6OOHm0I0gcOK2wfBxpdsILQintSlw24IojZBAmLRpQXM2SktGlFyDZCASFouEAs3Sot4FALU1gxK8Ip7QR3JXI1FOaLcEWLQme3ZFOZulbmos8EWFCZzdlzTuj3NuOC8GeCLChNIzZA0bJVI3s8EAt24JRhKWID2bbJWWoDmiyi0NERdV3rhiF+F0qIHcvafBLJocSdS36q82EXSoM8F4NVNjRQn6kIQiCUhi9p3SWWBmARhuPYe7uqor/3wtONabclmrBrDGaI2P9ajP+ILTFjYceHJWY37RXkIHm+skdirmMkaWsAbp7ikFDTtncTKLG2xAsmvONFXDN1eI3FsQYJA8fSPciMHzBJLTuhkjfE5nYcXNO/81T47nuZ2sf8ACto8Ooo3V0bI3jxuUVj+EwU0fXtOpzSA9t/bTZ6xI+p6xsh1Fw35WS/EMSp20xgqnNMmguc69w0crrVBxSMubHkk+GV1jcPq7w6FjmymcPbo2IN+C0JlCt/SGWqCs6zX1kLTfvKzl0guqqETVkbY3vEewYdt+HvV/wDRZA6Do7wKJzHMcKRpcD381bGSZzcuKWKVSJIeK8unZe70xUcRdj3IfeuEIIbAHdAeLngjSLIJG6BSs/cF73BDsvWCQu3AB5Be93uQiBdQvpezczKGV3zwuH6QqSYqVvcebvchCyYnzv0oZbyvVy0DnSV2IRjtQQDZh7nHkqczh0tZixwPpqaQYZSO/s4D2yP2nfyVb1VRLUTPmmkdJJI8ve9x3LjxKJ1W233Vix2VqTFFY902tznuc4m5JNytweidmN2aehpmFTPc+qw0mncD7Vhu02+66x7knB466R1dVsc6midZu3tu7vJXl6L+PnL3SvNgD5Q2ixaO8bDwDxwsuhpKScSnUftCyw6/pOyhheY6zL+M1rsIr4H6f6UwiJ/i142PlsUTiOMYXjEDo6LGaF7CN5GVMdiPeQmb0oMr0jcegxOelZLFO3S8ujBIPcqLlyhgsj7infGDya8/5Cq1mljutdNF+n1MnjX2SjPGZMMwyHEsGwqZlXVVsYpmtgIfpu4G5I53UdwHMWM5TxWNmKYa6W3Fk3ZNvtc0vy9hmG4JVNqKSjj61o2kkBefipDjUsOO4a5lVTCQt5EcPELnPDGLo1Syyl2STLvSVlnF5I4JJzh9S/hFPsP73BTAAOaHMsQRsQbghZXzFgU2GB1VBIZICTd1+1GFI+izpAr8IxWlw3EKh0+G1DhGWvN+qJNmuafhceJKdxozOXJoQtNkU4bcEodubg3BPxHei3pVySEBpIQHjbdHP4bItwNrqQCDxQC3dGuA1FA+kosAlw2KA5u3BHlAdwUgJyOKKcPBKi3bdAc1ABGnwXNPgjncEFLYBMjduCAW7cEok9lFocgCHN8EAtR7uKCkHUQgt3XNO/BHlAc03SNj0FhvgvBvgjQ2y6GpSxAA3wXtG/BG6ShBiQajuFttilIRyqIz/iC0wGkA2ve6zfh7LYjSn/3o/wCJaVLdr+IT4HdlebtFR5yrpIMXkp3sqYpJHHhCXi3mEy4LNStmkhxWMwykExEk9v3d/BS7P8MNW6o65rnNjF9LXuYSfNpBUBNBBWxCWR0LNOzIo5JJHC3MucdlEZpujr6bK5Y6oVYo57pLRXDeZ53TaI6eNtRPWSRRwxtvM+TffkLc0pqJHtgjZHGXkmwI3uOd1DM2VLqyZ8PBkDQ6QDhfgAlumaqTA1GKU+K5hhp5rRUT3BrSRsT3+C1Hl1sLcDoxTvEkIibocDe7SNissMy/PVVtE+O4ijcHyPLdgwC5Clfo5dJ1FBjNXkDFKgMhFVIcIlkfsQTvCTy39lbMcbjaOL+QjWU0U7iuFeJJte+rha1t+YXPNOjn2esvLx4rykU4RdBdshrhCAK2MZtexsvdWbc1lE/6apidWIY4B41rW/mgnBemCoPymJYqL/WxO34OUbGNZq90TreyfeFk7p0zI/MGeaqJr/6JQuNNC0c7bOd7zuiMSyz0kUlDNW4hi1U2GFmt+rE3E28N1Cnuc8uc4lxJuSTuT3qVEiwLvZQdJJDALuJsF0u5J1ylTeuZjpoyAWNJkII5AK2ItFg4FTerYLDT2AMbRe3eUGsqp8JxXDMxU3Ymw+pbISObQd04UAu1xO5Lr+FuSDXQsqKSSFwBD2kHZacdxmpIh9NGoulKkhzh0VU+NUoa8ugjnjJ3texWYrEmzrcd9uavr0VMcGYei+ryzWuElRhT3U5DtyYz7J7/AAVN5yw2TB80V9A4FoZKdNhyK3Z4Nw49O/8Ap9GTA1DI02NYGkpdRyFslnfNvbukLGvedLe0TsNkbsYw0bkc1ycqtm6MvoLxKlic10ctnxv7Lh3gqqMyUDsIxp8Nw5jbSREc23uPhaytetOqL2RdvO6rjpEIdi9O7heAXt5qEv1oJJWXvhnSjko4dTCpxuGObqWCRrmuuHadx8Uo/wBJOR3Hs5io/fcKosj9HuE5my/HiTqqZk5e5sg5Ag/9k8P6G6TjHXyHzJCzuSRKLHb0g5LfwzFQ2+2UZ+vWUHCzcw4ce75Syqufof09oVBcP+MCfvCQ1HRTUsHyZmd5PjKXyRLFBlwHOOVnezj2G+fXhdbmzLTvYxzDj/8AOFSbujWqa4BzawDnaNp/BFSdHr2e1JVD7VOCjyRDYy8hmXAHHbGKA/8AztXRmDBXcMUobf8AHaqGkyIW/wC+ub4Op7JNVZLdFHI4V8ZLWEi8dkb4hsNAnHMHd7OKUJP/ADDf5oP6Vw9x7OIUjvKdp/NZXEWrUWloA42CfMJytU18AmiqomttezgbphI22aO/SFGf98pz/wDK3+aG2qgcOzNCR4PBWfP1IxC4tXU1+Wz/AMkJuSMU4fpOhB/alkH/AEpbj9jODRoF08Z/tGf3ggtkZ9dv94Kg/wBRcdt8nimHEeFS8f8ASmytweqopjFLmDDOsYbOa2vJIPdshRUumDTiaNLhfiFy+6zNLHVs9nFY3+MdS8rjZKxv/qzm+U8iPGSpGmTc8ifcvG/1SPNZoE9dewxit82ukP5oyOpr2Xti+Jnyc4X+LkvhJczSYuV1rTq4H4LN7K/Ex/6ni4//AKbfmhjEcUB/r+KHx9ccPwUPC/slZUaSsd9ihN4LN4xrFI7AYjiLfOuk/wCy9+seLsO2JVQ86qQ/9SR4GWLKjS9ECKynJuPlmfxBaTlmjhhdLM9kcbBdznmzRbmSeC+bVNnDH6d7Zo8QmJabjXK9wvxHPwUjf0hZxzBSy0+LZhrainkdYw9YWsPmL7p8WCUeyvLJSNEdLPS9lXD5302AU7sXqx89Ix+mEHmA4+0fLZQbB+kluLVcWF4ZgUzamqPVtvpHaPK/cqeMgOlrTZoHDl7lMOiCKqn6QMONGNMsRc8EtuARa1/irFgjJ8DY9RPEqTJvV12JTSnS0U74iWvh1g33txC9BgtTXP0taHzSEEsANiRyKnkGX2VD6s+p9ZFLUmQMmfbQ7mRzHNH0zKPLtJVVOIsZFBSxl/X9YbgfV3QtHJvno2r8ljjD/wBiJ9LGNU+VckPjiDWV9ZaFgBH7x8gsyML4pBUMkc2RhD2vBIcHX43HNS3pDzNVZsx2WvnuyBpLKaIHaNl9hbx4qKzttGVuUIxjtRx5ZZ5Xuky5OjP0hMyZfZHh+Zo3Y7hzQAJSdNTG3z+mPA7+K03kXO2XM6YcK3AcQbOQPlIHDTLH4ObxHmvn4QRt+CV4PiWJYPXtrsLrqmhqWm4kp5C1w++3xVLgyLPozqFuO3ndeDlQnQh070+NOp8vZ0khpsTf2aet9iKod9Vw4Nd9xV8Hhc3SUxgd166BcL2of5CgCjvUo227N/chCkjtsxLrC/BDDQAkGpFedM8jKHo8ry0aXzFsY95WbeSv30laoxZaoKNpAM9SX2vxDW/zKoI8CngyGFHZyfcjT9RmKEbdtrm/EJiO5Tvk+PXmGC+4aHO+5WxXJBaFK/q4GjSd7gbJbSta5kw+k2PsC3MpvkI0xx7iw70fG50MBk1EE8wVsxyUZ2+hJ8/ElPo946ct9MEUUjtFHjcRp33NvlQLtU59IDJtdVZ0o6rDad721gLJHNiL9LwDa9uFyLe9UXiU09M2DFad5bPh0zaqN443H+StUZ/qDnXofp8dw6SQGWnZU3jce1YAkG3HdbsMuFX/AMf/AH0Ys62uMmUFi2DVWXsabR4hGyCUx67tlD9i3a9iQDe+yZ5nMZaOPbYC55+K5PI+SbU4Xv7Vzz8kVXtI08xbmsOogt5vxSUbYXUSHduq55qvM9uvjEYa4diFo4+Knb3bjtX24Ku81SNlx2bTwBa0eYWSarggszoFrXDBMQpzvpqA4DuBb/2VkipkPCwCo3omx3D8FqK5mJVcdNFK1mhzybEi6syDN+WZLaMdoDfh8pb8VnkmWIlbXFwB1Fd5HZMlLmHBXjsYvQO8qhqWsxWhkFmVtO/7ErXfmqWiRSd3C4Hh3I/lu42t42Vc9J2e/wBBwNoMMdH6/O3UX3BETeXvVL1eK4nWSvmqsQqp5HHtOdKTdPHHYM1PM6naLyyQtHe635pkzBV4H+iqsOqsO6zqH6R1rb3tss0OOo3cS495N1yzRyHwTLELbD2nSH8DfxV4dHmE4ZNlShqJoRrdFuQ47m5VEsdb4K++jaS+R8Nsdww3/vFPNcckxdDzJguFf+6PJ3/ZEHBaMm0c07R3EAhOGtcD9+Ky7EPuY3S5fj07V23LU0Kjs/RCnzbiEGu4ZJa44HZaEYGvcdQus/8ASN/5zxPYNvLw9yfFxIJSb7GqkbrfGy4u4houVY0PRjjBY1z6uns4AjSCeIVcUTrPi+0PxC1n0X4S7M+Hzy1VbND1IYAImtFwQeNwe5XZZSTSiRCndlOR9GdU0jrayb9yEH80czo7pYxeaprXD7DQtLx9H+Et0l1VWSOI3BlA5+G6qp4dDmiKJ5kdC2q0OY65BbqtYrJklkVWWRhF9FfyZNwOmb8p60XdzpgF6HLGBtF3UIffhrkc781qqLAsHALmYbRN+qDC381WHTFh7WYvQ+q08bAYHF2loaL37gkzQnGG5MsxuD4K4pMFwGOx/QWHvI5vj1H70700WGQgdVg+HM7rQj+SBHR1DeOkXSllDMeMgusjlk7s01HqirOk+Rn6yyGOGKJpjadLGgAbdwCZ8J9kEfXPLwTl0rN6rMro73IjaD8E34MP6O3xJK7Gn/js52RLeK7u1kq2fRyw98+ZamsDexE3QDbnuqmaVo/0b8O9Xy0yseztTvJ8SrorkSy1qmGOmgEzHx63E9i1zcqg/SMzPo6vKtLMS8/K11uPgxXHn3MMGX8HqcUqLaKWMuY3nJIR2W+Kx7jOIVOK4tU4hVydZPUSGR58VoXJW4qxvmOlx1WsTuSkdS4Ws3glhkBGlzN/FInA9bpa648EMAcLdfHvRhY2/IIyBlg3mSUI2cdBAAHFShb5EckexLRuRY+PctI+it0l4tilW7JWOzuqurpzLQTv3eALXYTzABNlnSo2a4tGw4KwPRcfp6Z8KA21QzNPiOrP8lVkQ6ZtS42XroJ4X969dZxiqDG4dqxI8AUVJPDGCZJ4Ix+3IBb4rMzspZ1qiTVZjfvxDquV34LjejetlNqzHg6/Hsvd+JVdf2WxXJIvSSxGnqsWwulp54pmxQyPJjkDrEnw2VSO4J2zNgsWA4h6iyoM9mh7n6A3jysmd/Pu5LRBKip9gX8VIciwtOJOqHO206R5lRw8+ClOTorRCZ2252HHzVkQJtC4ySakfVyWjZD3n7kmw97CT2raUdGeun6w2IbwVwjO1bR6sKd3CTYjwsr09FbFW1+RcVyVVSF0mHSlsYP/AOF9yD8bhUdK4y1dxpAYNlJ+hTHf1d6XsLmkcW0uLD1CovsNXFh+IWnTy52mfU490LG7OeFyYPmaroXsLTHKbHvBKacQ0upWSEm4dpIVvek1grKXMFNi8Y0sqGFj7i3aBO6p+pN6Q+aTVRW/cvfI2mnvgIHvDYTI61msJ+CrGrl66rmm+u8u+9T7H6j1bBKl97EMLW+JPJV2Hbm6xZOzSDhppKt3UxXLuI3sjxgGJadqV57tJB/NEQsMsgiY0lzjYDvKWOwnFYeEEzfsusqmSgg4HibBc0MwH2EScPqo7h0MsYv3FLAMXh+lVt8nH+a6+uxYDtz1BH7V+CUYbJGv1EOcTbjcoOk96UaHSEuNy4m5Q20srz2YnnyaU6YrTEdj3rjw+2ydosFxKfsx0NQ6/PRsl1PkvG5yG+riO/AvktdDdBtZGWg2PPbdXp0Yv1ZMoWtuSA4EeGpMOFdC2JVVEyZ2N0kEx4sdE9wHvBRjuhnNkItS41QPF7j5V7Lqqck/Yyg0T/U63A+Oy814txVdno06TKb5iobIBw6uv/nZAflfpdpRqFPiMgB+hUsePvJKSov2TtZZkJ3J3tZUF0k3/XPESPrjf3KVvd0s0O78Nxb3UYk/BqgWYpMTmxieTFo5Iq1x+VbJHodf7J4KcaVk1QTSf2fDYi5K2n0F9WygrI43EC0RNt/rLFVP7LLHzv5rRvRP0oYXlzDpv0vTvkkqHxtMVObljAD2jceIsE0/lEiHTNBUD8VbXTMrXQSU4GqGSO1wLiwIve/uVPY2/q8wkhuk+u8P30ZivTpgNPHM/BsGq5amwEUk1mN47ktvc/coJU9IFDV1IrJqKds3WGYhoAbqv3FV5lJpUNja3Gjcao4KiNj5BUtnY4GN8IvxO4KgvSuHDE6Eu1C8DhZwtezgqxHTbjDQYHYnO60peHvp2F1+7yCQQZ1wupqpqquxJxnndqc+Rjtz5bgDwFlVlyqUarktUIqfZLW234Lt7JhhzJgcltOKQeTjp/FKW4vhsg7GIUz/AClCw7Zey5yiVb0vuvmg95iakWGtLKeMfs3R3SlIyfNDerc14MY3a66KiNmFvcAAuvp1/wCNGKfzDbk7AeZWuOiKk9TyrhVOZGs+RDnE/ZFvvWR6UGSVsYFy9waB5my1RmPHIsn5FNVe8rKcRQMB4vIWqC4ZU/kVl6ROahiWPDA6WYmCjcTMW8HS+HkqjldpI5FK6uolmqJJp3l0r3lz3X4k7pvmcSTa5Vnoqt2EzyXdpadzxQqWHe5BQqen7RdILko2Z3UxuFgFBKs66RrHEt4gaR5oh/Zm06uW/miaV/Wzb8jddnNpC7vKCA14vFY81IuhHFP0R0rZdqibNNYIXkcmvBafxTA3tN9yDluQ0+a8NkBcNFdARbwkalydErs+hzXOLQHHtAC/wXnOsUAW1EtJIPP/AD5rxO6zFplbVugOO69qHeuOcON1nNiKl6Snl2apm8bMaD/dUbUs6R6Qtx91RtolYLnxsos4W5LVj6MkuwFtr23QoJp6d/WQyujf9Zp/Jeu23euqxckDlDj2JsYWicb8SWhK6bNWJQsDNEDzb2i1MSB/aAItoCV4HiuYcYxuOhw/S+eodZsQZ2R7+5WxJ0e41hgo6rEcWpZK41EbqWmp4rEPDrj5QnhtzTJ6MOFUrsRxHGK58kLY2iGCXRdjXnc3PLYBXRmV9HJhVLDUy1Ec8MvXMqaYXdE5pBa5vI/yVPne9cnSwaWEoclmZ4y7T5u6PYJ6l2iVzA89VZwZIBZwH1twdlmXFMp4wyidVdVEI2tc54EjRsHW1W8fBTvO/SRnyLKP6LytisFS6Z8nrNRNRaJomAai9r/ZBtccL7rPUdVjeKiGb9MTNiY4CRkkrjqdxPZPC60ZNSp1FPky49H4VJyEmealwoY6fYdY8k27goe1vNPecphNigia4dhhv3XJTKeyDwSydsypDnlmPrMwULRxEzSD71dTWNOxANuNwFUOQoteYYX79i5VwRHs+KpZYj3qlM/2qeI+bUF+C4ZKCHUcW45BK4tm8EdHx8EgyEFPl7CY7WoYj53KVMwuiaOxTxs8Q0Jcw+CG0CyXciwRDDoPFKqDDWGcbbAo5rbkaRdO1HFpA7KWU0SkxXTM0O0g7Bo/hSlt+9ER7SO8m/gj2lZrL49B41C1kdE/a33pMHcEZG4KQoVNe4D81lbp8P8A9UMTLrm4YePDshala4WKy50/2/0mYgbcWRn7lbh+ZTl+BCKf2GEcdW23iti4TgGB1uDUclRhtJJenj4xN37A96xzT8GgcL7+amjc/Z0mgjo4swVzY42BjWQ6W7AWHsi605IuVUUxkkuTSc+S8oyMJky/QW+toLR8bphxXLfRlStPrrqCjtxtXlh+FyqFbRZxxo/KQ47XX5vMrh/iKcaPo0zZUnU7Co6e/wBOeRrbfiVW8UvsbyR+iV43QdDccz+rxusJ4/0aV83/AE2UQxSTo9huMNqsyT92qGJrfvsU+0fRFi0rf6Zi1FTnuja6Q+7gnmi6HcIjOqqxasnvxDI2xj48VKgK5JlR1NXGJXeqslEd9jIRc+dkndVTbloY0+AV/UnRtk6mIJwx9QRznmc78LJ6pcv4BRWNJglBE4cHCEEj3ndNtX0QZrpIaqonZOWSvjYd3iM6R5ngnaAkuIVw9KMjIMpzRxtawSyNZYAAfcqcgNpSPBXY1xYshwwVv+t6W4v8vHw+2FP+mPMwxnG48Mp5Q6kw8aAQfbfzcf8APJVzSVElNO2eO2tjg5vmDddfM6V7i513uOpx7yTc/ir4NFMuzs5LjZo4cwihGUc3st8eaA97WsPensg453VhN9dNx7SMme8EnX7k3VEpfJpcSQkGXQuwk6nklDqm2IPJewpoDQeHNDrRt5KaF9noXEs8EsynRurM74PTtaT1lbFcDuDgSm2meTdpJCl3RDIxnSlgIe0Oa+qa3fkbG33pZdDo3Gwiwte1v5LzjdyC08LcLBduso5lFzu5cLibbpNLUwRtu+ZjfN4CRy45hsRAkrob/sklZ6NVjBmxsc1YY5GgtUYnweBwOiRzfPdO+ZsYoX1PWQyF4tv2CmiHEmzVEcUcDi57gBc+KhOakM1BxElXgeIUzw11M/SRcENJB+CRSQzRuLTE4eYKtwOMb4/o7BpHijnQRF/WFjCfFoXQiuDE2inhBMQLRyHyYV11HWNOp1HUC/A9W7+SuZjQ0cGj3IDomuPskEnje6agsO9HfMseE4biGDV2JU1C+edskNNWwkRygiznFx8BZWnjmLxUlA9sFaKKAi5NI4VEYv3WNwqbxXDKKugDaqnbJo9l52I96YX5Woi7S2oqg3jobJcW96plh5bNmHV+OKRaOaMwYXTUb6mTGcUreraXGK7IWlttwDxJPD3qmMnStdm6fDYm9ZHiLSyndK43jLu013ibbJZimWKODDzNSteZWEfOHUDv3JrIbQ5kwx1JPAaymnb2Itb3bPDgTdtrbkWBSeParJlqXlaTEGZqOpo8YqY6yKSOU9oBwsCORHhZNEntewLea09mXLGGZ8ynPVQNbTS0Yd1E7m6TqI7TD32Nx4LM1XDJBVSU0tg9ji0gd4VeHJu4ZXqcLx0/RIMhyUlPX9fNURsc42AJsrSp66lkDQyoiPdZ4N1SdH7acGF1wWtdtzAKskuSpJl2QuaeP3FKI9PeqYgra6Cxjqahh5WcU5U+acag/wB+125SAFVkqy3WcUYw8VWlLnnE4/nqeCUd9i0p0pM+xuPy+HvaO9kiV45IdSRYNGwg6inSHkoVRZ5wN4a2X1iE/tMv+CfqLM+BTgaMSgBP1zp/FUyTRco8D8HDrT5M/BGNk3SGCqpppCYJ45ey32Hh3LwR7XHxG11Ux10KOsRkTgRxUUx7OWBYOXR1FUJZmi5jj3t5ngEwU/Sjhs1S2NlDVlh+mCDp9wTKMvoHJL2WcH2Hesy9P3/3IrT3xx/grhpOkbA5CRJJU04PDrITY+8XVJ9M+IUmJ55qKyjnbNE6KOzgeJsrsUXuspyP9aFXQTQUOJZyFPiNJDVwikleI5W6hqBFitCU9FRUrAylo6aBo4COJrfyVB+j2bZ8b40Uv5LQN72Wwynbu+sUW7ghopzt7WUSA7v3rhcbDdecbBF6tkpKYJ/NBJ2QdR3Rbn2bugYg3TDI5uEUcPN05d8Aqsj/AKwT3BWB0wT3raKm37LHP+JVfw/OPV8PgVzfNA55OrabDc+z4JPSSFri65ueKDUOaJu1cnuQGycQFMexGOLpmkcd0UZIyOO6TNdqPaBt5IToO5/HgFY2KE1L3SHSxqLbCW31uBPclPq8w2a4WK6ad+kl9r+CKJTDcOb8mh1XAhAogWx3vzRs/aaSm9EexuY4tf71Isg1HUZ+y9UNNi3EYDf98KNy+3dPOS/lM34K0bn1+Ejw7YVc+h0b8a4aV7UiGu56v83XdaylkUfPsucfae63iUEOtwXCvBRQ9nX6Xizt0qyvhsc2P0zrmzCX28kkKfMj74s5x+jGU8KsVkvqInOZZvHiPPuRkJMzA/WI3t2LSOa7ZxALX2N77rjtMgBc3z3stCVFTdhzmzXFyy/ghaXH2re66JY/TZuu48Ecertwd9pSAJwjdGRubcd+CRBnynM+YR75owLA+aLcdr7hFWCEmIROlo54WSMjdLGWhz3aWt8SVLsh5BwehaMXxXF2mrfBok6mBweQQPYJuB9oAlRGTU51m6dV9iW3HvCmmWukCnhbFhuLR4jh9S4iNs0MzfVZDy9r2D4LNmjLpGzR7LbmSTEpab1L1d7pKDDIxppqWNhbJOObtI3bfv8AiqZzVl7DanM1bI2jMQc8WjMnsWG4NuatPMUown1rGKyUUtCI/lppazrauqd9GNlnEMHk1QLLjW4hStxBsZZ1r3OLSb2Nzsb8+8rlarI8UbR2MMFne1jVQ5do4h2KdjT5JYcFZbstaPABSWKkANuPlulMdKOJH3LlrWTa7NL0kL6IbJgdxp0og5ba7ixvwU/bSi97IYpWX5/BH+bNeyP8SH0V3+qrXezqb5bLoymfoySNPibqx20YA9kobKIn6P3Jv8/I+2Q9Hj/4lbHKtc3eOoYfAtQH5exRg+ZZJ9l/81aHqe3D7kVXU0sNHLLBA2aVrSWRn6RUr8jNUVP8fj7Kt9QxOndf1Woj3Fi1pNv7qA7G8Sp45WNxGqjY0HW0yH4b8E+V1Jm3EqiOfqjhnVg2YxxDSe824qOV2Xcepqh1TPTOqzqDjbtBx8iujj1Cl8mjHl01P9RNhGCYljRdUzkU9ESTqkudQ8L8fNPUeF4fhcb6htUxlMW2a57Q6RzhwAA3t4pE/M2IMkY18cdO1gt1EkBLb99jsmOqqq/Eagu6ued5PJpI9y0Jr/kqKFGv9bYoxTFHzucY3WHA6eBUPxp2qtc47HSPxUtpsuY7WOGnDagDxFkw5ywuqwjFfV6oWe6JrrcbBX48uNvbF2ynNCdbq4JR0Av05/i3400w+4LQRfy5bfgs8dAp/wDH9P8A8vN/CtCHgr0ZT2pced7oLSuPcFEgOFxsuA7rhOyDq7kpKR1ztyi5dwvHjxRb3bbG5QMVP0pTdbmhzNz1MDW/monGRpce8p/z3L12bK91wQHBvwFlHm+ztw3V0fgiqS5sMmp45Qx5BBH3oPU2ddnNGR9pgF97LsDi15Y6xViVCs5HubEi45WQtDdV+KBMzQ/W0oxjg4iyZKxT1x9UBFyu7B09yMHii5uy027lPQAKH5k+aOf7JSek+ZvwuUa6+lTYLsb59nlSHoth9Y6RMBj5euxu+Bv+SjtV86VO+gCj9c6VcLNhaImQ+4EfmqJS9FiXJs5p208wug7Iok2Xg4qgtSMA6l7VsgLyakAO+6f8jt/pk8vJrLWUcuVI8oQydVNLqIa5wA9ymK54B9ExZJtb2fcvOcdR7QI+yiI45nNA2AtzNigyRtbt1j2H7V7rTtZQmg9g1u3FvIWQ3Mbbi/zvwSeOJ5bds7x5ELphLXDrJtXmU20m0GtjINxuPEoucuBsL28kZaZ1tMjLeS9JHKQNxZCX9BwJyARe77fii5mMfG9kgBDxZwPAjuRx1+zcBFvY0+y/Ub+aHFv0RuQ0wYVQ09YyaOnDNJuDyCYXZqxbKuIzUFKaeWl6wyNbIy/Hx4qZuo5HNLjDMWjj2VWOdamirsTEtGXdhoY+7bdoGyyZsC6lEvxZ5RdxZOMN6WYyQK/Brd5gl4+4/wA1JsM6Scp1IAlnqaRx5SxbD3i6odtPIQC16E2CTfULrBk/G6eXqjfj/I5o/wBmn8LxvL2IAep4zQTO7mzAEe42T0ynaQHNDXMPBwssgue8G1ztwTjhuO4zhrtVHilZTH/25nN/BY8n4Vdxkaofl3/tE1iKXb2Chtg22bZZ0wvpWzjRuGvEGVbW8BUQNf8AeLH71LMI6cahpDcUwOCUDi6nm0E+51wsOX8TqI/Hk1w/J4Zd8FwiLYC1/NDFOLezZQfC+mDJtVpFSa2gcRxkh1tHhdp/JS7Cs2ZTxTSKPMFA5xPsvl0O+DrLBk0uoh3E0xz4ZcqQpdTsd9Ft/JJ5MPaXewLnjfmn2KNkrOsiLXs+sztD4i6KrDTUsZfVVEVO217zPa38Ss22bdUXOUK5YxS4HTTfO08bzf6TQUFuEwx9lsbWAcmtASbFukHJWFgtmx6mmeOLKa8jj/dBH3qH4x014DCS3DcJrKt30XTOEQ+G5WzHpNZkXEeDNLPp4O7Jw/DediRdUX09U5hzXANhelYe7mlWK9MuZqpxjw+noqO57OiIyPHvd/JQzN1VmKvrmVuYm1RmkjHVuni0XZysLDb3Ls/jdBmwZN82jna/W4suPZBD90FOt0gUg74Zh/hWhA+4Kzv0IuDekGiLja8co/wrQDni/Zdtddw4YfrsgPft7Sb6rFKCl+frIGEd7rn4BMtdnLDIr9V11Qb8GM0j4lAEmLvH4oDnbdygdZnSsfcU9PFCDwc4lxTHiGP4vVX66vlAPJh0gfBRSCyzquupaVt6ioii3+m4Jjrs5YLTus2odO7cWiYSOHeq0ne9ztTnlzu9xuUmkcS1xJ5W4oom3R7Eao1VbV1haQZXudZ3K52TbFq6k7C+5CUf2BPEcEUPYsrY9UI2djk0xRue035o2QNcA9qC13Z3Qmns24KwWR2B+oEO3K8x8bXbbFFua5h1N3ulUbWlgJAumiKc1MG+sImoc3Q6zgbiyUlrbbtCTVBaGOsLKaABTXbTtv33RrvZRUQPq7eKEHXbxQSIar5xW96KtC6bOVVX6SWwwgA+JKp+r3lK0n6KmFeq5dqcQc0h07rgrLmdSpFsFZeF+Nl7mgXAK7qVY5gNcK5zXjwTgzxPDdWR0U0MVRhs8skJEnWjS6RvZc229lW34K8ckU4ZluhjDXRy+rtdbhe5utejhumrM2pntgO9Ph9EGjXRxxk82t2SmWjjjjJjjhLed2IxjSGgud2h3I9/aaG6jY7m4Xa2RXo5blJ+xrmw+nDu1SwOBPHTZJ/0ZRulOqii0342Tg8l99O9jYnvRYnbGS0XAaeanavojdK+xPUYLhsbusbThzuGi5Ufo4o5cRMb6M6HFw0ngCpNiOItpeoqGta9rH63c9rHZNNJiMNZJDXSRNiZ2jpHMkpVCP0M3P7FkeFULnginjFxbhwSiOigjLurjaCP2QvU0kbnfJu24g8PcjtembjpJ48wVLivohSf2ETAFro7XBFjYd6zLi0XV4nVxaT2Z3gfFaYq5tIfYDgRss6Zva1uP12h1yJnHbxXN16/VG3SNtsQMa7qm+S8ZC0W1ADuSVr3WDO0b8N16TS3bSdXMOXNNzPS6QSdVyjY5Kd0WmQDV32SYtLjc2C5pN7BQQLPV4nfNvQXUctrtc0jx2Se0jBzCEyeRvBxI8SgDroZ2n2XfuoJkc3ZzeHeEc2tk+k26NFXA4duO3uuh8k7mKcHx7EsNkDqOtq4gARpjlIG/hdJazEa6sk1VNTPO7j8rIXfiUINo5PotHvslMMcLG9hjPPml2Ru6H8k/s9lzDJMVx6hw1z3wtqqhkWvRfTc2vY2WjctdDHR7Taf0lNiWIVHBwnk6qNxHMBu9vMlUbkx2nNuEO4AVkXL9paOxPMmCYXVtpsWnZE+T2diDbz7lTmTdJOgg/smmWstZbwAasDwTBoHW9plK0u+JuQs/emDUzT5ywqSRobagsC08e13K38HxfCqo3o8TjqGHgOvDj8OKov0o5OszlQOGq/6PbcG3e5Z8OFwyW3ZfKa2UivckV1Th2YKerpZNEzSQDp1cQeRU8qsZxOrv19dUOabXAdbkq4yz/teE+J/BThpFj7vwXQMgY3fc7nvXSfegtIsuX3QAI8CiX8EZcImU7IALl80mndpgefDijnlElw3Fr+BQAbhND1+BzVc3W9kEggiybmjsqaQOgiyFVuikaA1oBaOLiTw+KhBOkC/clwz3No16rEsajX0Ggi3ELl7P23RUZFka2y1GCQY57rdmx8Fxk5ai9VnWKMOm3BShQ0SOc32knq3OI438kK7RvuiXu1zNt37oYyFIFoAPBENdp7KOd7ICTTcTuoIE0mqSpaxt9TjpHmVs3ogwwYTkehh06XPjDisjZOpP0hmzD6XTqD5h9y27hcTafDaeBtg1jGi1vBZZ3vZbDoWXQmnbiikJpACgcwQvHggXXrpwBAFztLRc3G3iTZaCy9pbBAIzZgjaB5AKgKGfqK+nqNNzHK11u/dXTh2IVGvtbDkLcF0NFF8sw6t8ImMTdIc7SBq4XXn3a27rNd4cERS10c8TdTO1bihYg9rW9l1wRZdWuTAuhFPVujY/S1pN+NkkNTK9/aZGAeZRdRKWlzGt1bcbpI+onHsvjjFvq7pxPYgxzEC6eKiY1gMhJItwCNw/rIWlrWtYA23zeyi9RiDKzGK1rXbQydWx3c8D89090mJOkgbedwJaNhwuohJS6HkiQUczwR1j4zfh2LJa9zgQXRgD8VG6WomdINcpeCdr8k+uqS6BrS7gmk7QqZ6reS4auywrO+ayw5jxARm7PWDY+9XviNQerve4F+Cz5ikwkxGplNhrleePiVyNe1SRv0kXdgKVo68EjYXKJkdqkLnHmjqYnq5Xn6tgTsj8MwXEMS1yUsRdGGlzj9UDmVzmbEID7S4eHJLZ6Knp4nufUdZIyw0M5nzTcC2/aBv5qCQ+GQtItw7krMMMnaMfFN0Zs4eadGX0i1j70AJ30TT7LiPAop1HK0dmzvJL0No2QAzuikYe3G4e5cEjm8HEeSeNPiUF0Mb/baCgA7JlZM3NeE3dcCsi4j9oLWQhoKuBsVdhtHVNbcDrow63vI/CyynlykijzHh0jdQIqoyBf8AaC1RAbnZxG5VGaTVUPHkQVORMm1WrRQzUEh+nTSltj77/dZU96S9LFR5gwilhmMzIsPDQ4gA7Od3K+29Zpvqa7z2VCekzf8AWTDHabXpCP8AEVEJNslp0Vrl02xSD7Sm4UGwA2xOD7am4ddaCsMDtuC5q9yDqXNSmgB6tkVI66EXbIB3CgAl6IfztxR8myTvPFAAKatkjoa2hc5wD3tcN+IBukcjux7KHWObHMyRw2IsUULSDY7KYJJj5MjkkGR/NoWorjOyR4tXSrrRRI6/iChBx0rh3aPgg+CkUG8i1kCAXmLuIBXJHWHNH4FRzYhX9TFIGNO7i4XF0PgZQcnSBuLbEjkkNW42NlJqjK+L6fkYo5/+G7f4KOYnSVVIXR1FPNE4cnNS2h5YJx7RNfR7w79IdI0DizUIGGQnuWuWkWCzB6LLWHOVa62r+iX1W2HaA+O600x23FUslB4KEi2243XiblQx7MAPrqdt+0Se4BETYkfoRW8ym5csVJDdj9leWaszDRROIDTKC4DuCuqnJFU8jc328lTXR9C6TMsAHHS4hXPTNtVluwt3rq6Jfqc/V9pD7SOBYCTY9yMe86eD2jja10VTWINg0utsvP2jDpJLbb3XR9mMQ1RN9xx4A8k14rUR0tBNPO46Ym6+4bd6cJHMlJk1dhvEqAdKeKCDCxQx/wBu4dnuaNyqsuRQg2NixuUqGrJEzqqCeaYAl9UXk+JA/C6l1K0se9lrjURfuPeop0cRiXCJ3AjVHUX8xZTOoLQdTQ27mXv5qvSN7CzUpbuBVRnW9tvocDyTozW5p7eq3cmvDmlsbdgQeN06BzWxu4AWWpmehtxSUxwP4g6HH7lSrnUVmyROPXOLg8SM2uSeCtzNMvq+EzynYtge4i/gqPdI4uJtpBPALi62VyR09MuBVJK31cx3Lnu4nvXaPEJqI3p3Pa8tLXEOIukZcT7OyCAViNIsnrpJaGOmMcYEbi7UOJJ70iO5RjWO03vshaW9xugApCa5zQNLiEPQPqrzohbbigATKuZuzu15pQyubsHN0+N7pEYyEE3HHZADsyaJ52eLo1nBMiG2aVnsvcEASTBXacYonHlUM/iC07E5oHaBO53Hmsn4LXSfpWjDmtPy8e/7wWq4HODbtBcLm9iqMy6Hh2L4nN2LXkd4VH+kvvjmFE7k0zt/31dLZI7dtujzCpP0kR/rXCCD/u79/wB5Jj7LH0Vlgf8AtKD7YU1bzUIwV1sSg+2FMy9a49lAaV6yLEi7rUsAZKCSgl1wuHzKUADzuiX80N58UU7nugBHiMZkhNjuOARdGbwC+xBslvikkbrTzM77OQuwfQZJfQHje3L8lxjmyDU3h4967H7SKnjjN3NJaedjZWJ0VsUNBsuHiSkkUkrY32me5wcLat9kq0uO9xZNuIoLkOymWQcL/wBWuxOTQHPqRGxp+kCNyogGDrG9Z2mXGpo4kKZYPjMVZPDh1HTikpaVmpgJu4uPE3VWRujf+PhHyclgyMbR4bI7SwvaAe1yJ7lBc31Jkw6pAa02Zd1u9LnVdVJR1EUVY6Zkr9PaHC3cmnHGt/RWI65rlkQbYt4KmM2zsZ1Ha0T30XI2Q0FadI1yG4NuX+Qr0a4W9yrDoFwOPDcn0le4StqKllz2+zbyVkscrTzz7FYOy6D4olr0YDdBCPnqGNBKFpbbuXV4gcFDslEo6L2t/WcOdcaYnG6uDDOyx01r35HdVT0XUsjsQqqvYNYzq9+91yrUp39XE0Ds7WA7l2dHH9LOdqn+44U7tJZH33QsQ06DHJfSRY2RWG9qZpJ3G6DiUt5nNPC62+zKNT44qSmL49RbewHeqh6QK/17H3wixZTjqxbgTz+/b3K0M2V7aPCZpA4N0xudv3j2fvVHdqWVznuJc4kknjdc3XZOom3Sw4cibdGUmk18I2Y5oNvGxUze06msvuAALqAdH79NdVNvYmIH4GynssgdK420kC4+C0aJ3jRVqVU2O1D81c2JHLmjhd0Egv8ARNk30Ty4u9oE8glmu0bm6tw3uWyRmId0q4iKfCo6NpvJVHtW5MH81V4GogHvUv6VJteOwxC3YgBHvuogAWtLiLWXn9TK5tfR1cCqCOO7MpB4XRtmGxvZE9p5LkKI72cN1QXBm3Jy6PO69pH1QuaR9UIA6vLgZuAALlddG5h7VvigDy4dzYgIRC9c8rIALdG1w2uF4QX4OKOjaHHe/wAUYeyOzuO4oADhtPLHiNI8tu3rmHY3+kFqqkmbr+Ua4t8LbfFZowqnY6eBzgfnGkE7Wsb2+5aMgebtBN7BVZfQ8Ox6Yad5fHDVRl7dyxxsQqV9I9unEcH5/ISb8fpKxaHB+pzZNjcdW8MlhEZgA2B773Vdeke/XXYPfiIZN7m5u5PLHCM1slYzZVmFG2IwfbCl5fdQ/Ddq+A/tqUl26dFTDtSEH7JPqNua7qTVYth/WeK9rRF1wu8UrCwx5uiXEX5LznbIvUoGOlxvZJpezVRu+tdqPJ4pLVGzGu5hyF2DDTqFyNlwm7CuniUEHYhOIFjaUt+s3fzSmJ3yYBSU+1q53RzDaQ34IAO5bI2gr5sOqOup7Xc0tdcIoG4RUguhqx8cnF2h7pcwR0cVMI2ydZFIJHO03BI3Tdj+OVWJTVcjg1vrJu621/ckDzZEyuHWXOwFr+A4/kkWNI0z1M5dmy8jAQ5TwmOwA9UjsB9lPTXbplylNDUZYwyeAgxvpInNty7IunXVdM0ZbFQdcbI1jikbHbo1r7JSUYOaBoGwQZeC8vKV0SuyedFf9Qk/4x/AKwIl5eXb0n8RztV8hdh3zvxSbFPnnry8tJmXZCOlH/YLvJv4qroea8vLja35o6el+DJLkT/aFT/wf+sKcn+sjyC8vLbof40Y9V82L6L+tSfYS4+w77IXl5bpGUqTpF/8zzf8Nn4FR2T5py8vLz2o/lZ2MXwQGn9ly4/2l5eVJYGx8AvLy8gAM/shcHzh8l5eQAauFeXkAHRfNDzXea8vIA9hbneuw9o/OjmtL0/te4Ly8qc3oaHYti5KpPSLJ9bwnf8Asn/ivLyrwfMaRVuHf16D7alJ4jyXl5akVs8VzmvLysQh1AdwK8vJGAWeCAV5eUDI5yRFX/VHea8vIXZL6DWew3yXF5eTiADw96Md7XuXl5ABkXBccvLyBkJpuKIl9o/ZK8vIJ9GtOiAn/Rtge/8Auo/FS88F5eQxPZ5vNDPELy8kHP/Z";
  private static final java.lang.String externalImage11 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QAWRXhpZgAATU0AKgAAAAgAAAAAAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCABXAHgDASIAAhEBAxEB/8QAHQAAAQUBAQEBAAAAAAAAAAAABgADBAUHCAIBCf/EAEUQAAEDAwIEAwUEBgQPAAAAAAECAwQABREGIQcSMUETUWEycYGRoRQiQlIIFTSCosFysbLCFhcjJDU2Q1Nic5KTw9HS/8QAGgEAAwEBAQEAAAAAAAAAAAAAAgMEAQUABv/EACURAAMAAgIBAwUBAQAAAAAAAAABAgMRBCESMUFxBRMyUWEigf/aAAwDAQACEQMRAD8AE3NL2twfs3L/AEVkV4n8PUMxI8taZTLEkKLKyQQvBwcbUYpjelE8RtN00DKg5SqRaZAfQM5IaXsofA4PxpGLBN7T9fYfea500Y1H0xPgOeJbb1KiL82iUH5girVm9cUICPDi63uLrY/BIkrdT/0r5hVJrrXL1nvC7RarWZklBSlSySRzHB5QBv0Pzqy0vqNF2dEGfDdt9yCSosOJKebHXGd/hS/BJh+VNbHXtWa73Fxs2l7z6ybRG5j+8lKVfWoruqozn+l+E1mcPdVvflRj9HFD6VfOsDyqI/H36Uzwf7FOylTqHh9z88jSmsrI4PxQbshwD4ONA/Wh/U+p7amY2NOXG9SI5H+U/W7LZUk+nKVZHrt7qL344x0qBIhMOZC2UL96Qa3xf7B8kD7d1juoSf1tZHlkboUh5kg+8oCfrXxUgr9lEVw+TE9pf05s1Pk2e3L9qEzn0QBVY/YLac4jge5RrdtG6T9yNLlPtfeNunBI6q8E4Hx6V17YWi5YbevB3itn+EVx87YIaN2y4jP5V12lplgDT1uHlFa/sCl1/r2C14kZcXPamHInmKICyD2plbGe1akY6B1yIPKlV05H8hSo9AmbCMB1FeNLKRaOITKXjiFeWVQn89MqGAfng/CrYtDGDWecUtc6asiDDXM8a7RnEuIYYGVIUNxzK6J/r9K9NOaTXsHWmmmemtCWM8Q3Lm8H0S4c5xEltzPKpwfeQoHsOm3fFTL4r7ZqSC9Iatq5GXhysglbSRslYz2IzkiiOdqPTF10vb9UyZrFrkX2MiWkPLCcSEDw3E8xwDhQII286zqy6ktb2qVS7nMYihTQZiLKVJZd8+RZ+71yOtBmipy6ZVjyw+P/AHQTvRtulQno5zRA62FJBGCDvtUN5kZ6UeyFrYPvR/SoTsffpRA8x12qMY++cVuzNA67HOOlQXYpJOBRQ5HGOlMGMPyigq9IZMgwuAtXaurIExm3aYt7joJ/zVsADv8AcFc+JijHsith1ZJch2zT6+ZIaMZPMlYPL7Kd80n7jaeinHhV2pZcW7VkORNZiSGVRlvr5GiVBQUrsPMUQrbrDtR3Zla4McqYjSBLRyOqwke2N/cPOt5I22p0NNC+Ri+1eiEtrfNKpK07GlTCU5uvXEHUkG0ypy9DOsNstKWXHbk2QnA64AOfdXONlDl51Uh2a4XnHXlPPKXuVnJUc+81s3FXXFpnaFuMG3sXHxZAQgOOwnGkAc6Sd1AdgaxrRP8ArChz8iVH6Y/nRYltms3aygXLhzeLQvKn7RITc4vmWXMNvAegUEK/eoUuTDEm1uRJaMoUMgHqPUev/qr7h5dEW3WEBchwohyeaDKOf9k6OQn0wopV+5Q/rlEm0SbixNUoyIrjyHSo5JUknf41dmlVjVfoTFNU5/6WfDjUOrn9HQm4ES0SmI4Uwlb8haXDynYHYjpj4VcvX3XIVg6bta/VE5X/AM1i+ktX3uxQVRbeYhY8QrPjNLUQSB3T7qkal4g6nusduKmS1EAOVKhc7aleQJJzj3VzXL30UJ9Gi6i17qOxoZXcdMRkh5fhoKZhOVdfyVIGq9SgAuaVbTkd5hH/AI6xaKxqe7ymsM3a4+GoLxyuO4x371tKtRxmzmRGucfH+9t7ycfw0NJo1fBRSuKTjMxyIuwhTzailaUSycH/ALdOs8RZju6dKylDzS6T/cof4dzba5xz+0S5CG7e9LXzrcPIkoOepOMCt5fvXD+FqWQp+bZlRPsyUpQVh9sOc2SB13x3obS66ClszRrXVwDC31aRnhtCSpai5gADcndIolufGuVqtVhtFnsjURtJaYUqQsuOLOycgAgAd+9UN8ctqrFclx30LBjulPKsH8Jx0oY4GMsOa5s65SkpYjBUl1SuiQhBVk+44oONKyb2hmWnDTTND4rFOl7I8qetL92ntlmKjAw2k+05jzHQepHlUHhPx/vNgajWjVLKrra2gGkSU/tDKRsM9nAB54PqaAuKeqH9W6ul3L7xZJ8OMg/gaBOPidyfU0KpKEgITg74J9a6KxQlpE2XPeV7p7P0KsV5t18tMe62mY1LhSE8zTrZyCP5EHYg7g0qyL9EaYHeFr0fBCotyebPxShX96lSGtMxGHcQWzL0440hWFeIkj1wazIWy5tqStEOTv7K20k59xFWUy5T5bakSJjy0HqnnIFHdlbTHtkVKScBlOcnfOOtewyw8lJ9kbg1GgCfcn9WvKZS1G5Yjc1woSpRzkjm2JGBt609xs1RI1VdLvKjswVNI8IuOQmzjBAHMpXMQd04J8yKnvltTfKrfvuKftD1gt1tukiS26ZsllTK0qBKFNFPspA2ye+d/lR5FULaGYqnIlDSX9BHhRIhsQpQkSGW1uOjCVrAJAHr8a0mGzDcHiIbYUT+JIBoPsmjLPIjgxVtvA78zbvOPoato+gWUK5mnpDR80KIrlX9Rw772iyfpub20HtljNMDxAhKVK64Harxh9KSBms0kWXUdujg2y5XKQvOyFugpHv5qHJ7GtH3C64xdnppyAfGCGWx/wAIBwaCeTiyPfkgq42SOmiNBjXiRx11BJsht/2iOtxwialSmylQCTsncn71Gk7Tmoronlul6tbaDuURrQ0fkpYzQVwgTPj8Tb41c1KVM+xkulauYk87Z69+ta644fOumknKZzabVNAKrhlp5pLj8yTPkqAKlAuJbR8kpGKzODcHbf4pjK8PxkKZOOvITuPltW26olBmwXBzmwRHXj38px9awflQ5srP3T2NNxICz1KfSlJLZBUfrUdoKTC5z7QXk1KQ2gjHKDj400v9ncGO5qkSjqb9EdTP+LecUDCjdHOf1Pht/wAqVM/opNKj8L1uqBxIuDrg9wCU/wB00qjrtj5XRyu2lTjqG0bqWoJHvNbdE0u0mEjmkuZSkbViNiltOX6A0hQWoyEbdvaFdBs3ArtaVFPKvl6jpXS4OKb26RJy8lS0pZTSrK0yzzCWCVKwEqOM/WmLrYWWbTKkJkKU4hlSkbADIBxn5VAud5Uq+IiJyssthxYyfxEgf1H51MvFyUjS8pZAyGF79uhq6sOLxb0SrLk2k2YE26WCXGXXGnQfabUQQavbTrrVdu5fs2oZgA/C+fFT/FmqNIYzyPPcg3OUjmyaYZdCHSVAlPlXz144tapbOvOS4/F6NStPGjU8cBEqFap4/NylCs/AgfSi/Ql/1Hroylzb9E07GZcSgIiRErWvmBOApxRwduwNYOPsbo7JPyrZP0dktNxbyyrDralskpWAodF9qkvg8ad0oW/goXNzvp0yoel/4J8Yb+zFffuWGEth6U4CtWQ2okkADrtgAbVYzdXXmQDyvoYHk2n+ZyaHNb8jfF29JbQhCAhASlCQAB4bewA6V4LhxVkLUomp7bZ6myJlylNsuyXXluLSkeIsnGT61Tqw26pAIJSSM1Ilk78pII3BHnVchRKedWxPXPamQ+wa14/0mNuJSCOUY8yaYeXysE/mBryonGEfeKugHeiK5aWdYYgqMtpa3nG0Kax7JKgnqM9zTXaRmPFdpuV6HVHCG2/qThvY4BTyrEYOuf0l/fP9qlV5EUlthDaBhKUhIA7AUqm0Hs4Q0LH59VwAEjZZVv6JJrdXXUotx3KU5wMdcClSrscDrG2c/l/kjLNMXRc/U92lLJw9jl9yTgfSiDXk1TGjZHhjBdCWx8SAfpmlSrcdN8an8g2ksyXwZAkDmwR6V7xjtSpVyDoC8JKlYxita4AlUVm8FpIcUrwilClYBIC8b42+tKlQ31Js+oP8QF44r3dQzuhvr/y0VEDxpUqKPxBp9jDz2TUQq3UAds0qVavU8/Q9RlFIGDgjcEdqK9AXeRI1vYIUlSCz9uZB+7knChyj5gUqVa0mFF1KaT9TrBl8g70qVKhZiZ//2Q==";
  private static final java.lang.String externalImage12 = "data:image/gif;base64,R0lGODlhEAAQAPQAAP///wAA//Dw/oqK/uDg/kZG/np6/gAA/1hY/iQk/qys/r6+/hQU/pyc/gQE/jY2/mho/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAAFdyAgAgIJIeWoAkRCCMdBkKtIHIngyMKsErPBYbADpkSCwhDmQCBethRB6Vj4kFCkQPG4IlWDgrNRIwnO4UKBXDufzQvDMaoSDBgFb886MiQadgNABAokfCwzBA8LCg0Egl8jAggGAA1kBIA1BAYzlyILczULC2UhACH5BAkKAAAALAAAAAAQABAAAAV2ICACAmlAZTmOREEIyUEQjLKKxPHADhEvqxlgcGgkGI1DYSVAIAWMx+lwSKkICJ0QsHi9RgKBwnVTiRQQgwF4I4UFDQQEwi6/3YSGWRRmjhEETAJfIgMFCnAKM0KDV4EEEAQLiF18TAYNXDaSe3x6mjidN1s3IQAh+QQJCgAAACwAAAAAEAAQAAAFeCAgAgLZDGU5jgRECEUiCI+yioSDwDJyLKsXoHFQxBSHAoAAFBhqtMJg8DgQBgfrEsJAEAg4YhZIEiwgKtHiMBgtpg3wbUZXGO7kOb1MUKRFMysCChAoggJCIg0GC2aNe4gqQldfL4l/Ag1AXySJgn5LcoE3QXI3IQAh+QQJCgAAACwAAAAAEAAQAAAFdiAgAgLZNGU5joQhCEjxIssqEo8bC9BRjy9Ag7GILQ4QEoE0gBAEBcOpcBA0DoxSK/e8LRIHn+i1cK0IyKdg0VAoljYIg+GgnRrwVS/8IAkICyosBIQpBAMoKy9dImxPhS+GKkFrkX+TigtLlIyKXUF+NjagNiEAIfkECQoAAAAsAAAAABAAEAAABWwgIAICaRhlOY4EIgjH8R7LKhKHGwsMvb4AAy3WODBIBBKCsYA9TjuhDNDKEVSERezQEL0WrhXucRUQGuik7bFlngzqVW9LMl9XWvLdjFaJtDFqZ1cEZUB0dUgvL3dgP4WJZn4jkomWNpSTIyEAIfkECQoAAAAsAAAAABAAEAAABX4gIAICuSxlOY6CIgiD8RrEKgqGOwxwUrMlAoSwIzAGpJpgoSDAGifDY5kopBYDlEpAQBwevxfBtRIUGi8xwWkDNBCIwmC9Vq0aiQQDQuK+VgQPDXV9hCJjBwcFYU5pLwwHXQcMKSmNLQcIAExlbH8JBwttaX0ABAcNbWVbKyEAIfkECQoAAAAsAAAAABAAEAAABXkgIAICSRBlOY7CIghN8zbEKsKoIjdFzZaEgUBHKChMJtRwcWpAWoWnifm6ESAMhO8lQK0EEAV3rFopIBCEcGwDKAqPh4HUrY4ICHH1dSoTFgcHUiZjBhAJB2AHDykpKAwHAwdzf19KkASIPl9cDgcnDkdtNwiMJCshACH5BAkKAAAALAAAAAAQABAAAAV3ICACAkkQZTmOAiosiyAoxCq+KPxCNVsSMRgBsiClWrLTSWFoIQZHl6pleBh6suxKMIhlvzbAwkBWfFWrBQTxNLq2RG2yhSUkDs2b63AYDAoJXAcFRwADeAkJDX0AQCsEfAQMDAIPBz0rCgcxky0JRWE1AmwpKyEAIfkECQoAAAAsAAAAABAAEAAABXkgIAICKZzkqJ4nQZxLqZKv4NqNLKK2/Q4Ek4lFXChsg5ypJjs1II3gEDUSRInEGYAw6B6zM4JhrDAtEosVkLUtHA7RHaHAGJQEjsODcEg0FBAFVgkQJQ1pAwcDDw8KcFtSInwJAowCCA6RIwqZAgkPNgVpWndjdyohACH5BAkKAAAALAAAAAAQABAAAAV5ICACAimc5KieLEuUKvm2xAKLqDCfC2GaO9eL0LABWTiBYmA06W6kHgvCqEJiAIJiu3gcvgUsscHUERm+kaCxyxa+zRPk0SgJEgfIvbAdIAQLCAYlCj4DBw0IBQsMCjIqBAcPAooCBg9pKgsJLwUFOhCZKyQDA3YqIQAh+QQJCgAAACwAAAAAEAAQAAAFdSAgAgIpnOSonmxbqiThCrJKEHFbo8JxDDOZYFFb+A41E4H4OhkOipXwBElYITDAckFEOBgMQ3arkMkUBdxIUGZpEb7kaQBRlASPg0FQQHAbEEMGDSVEAA1QBhAED1E0NgwFAooCDWljaQIQCE5qMHcNhCkjIQAh+QQJCgAAACwAAAAAEAAQAAAFeSAgAgIpnOSoLgxxvqgKLEcCC65KEAByKK8cSpA4DAiHQ/DkKhGKh4ZCtCyZGo6F6iYYPAqFgYy02xkSaLEMV34tELyRYNEsCQyHlvWkGCzsPgMCEAY7Cg04Uk48LAsDhRA8MVQPEF0GAgqYYwSRlycNcWskCkApIyEAOwAAAAAAAAAAAA==";
  private static final java.lang.String externalImage13 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAADbUlEQVR42n2TW0yTZxjHv9t5sXC9eLWQsDhJFjDOMefQwcigchC+FmYppXbF6sC2TOgnRyeHltLDCrT1+3qgBUrLqV0pKhQZoC0VizBnHDLwtIE6smTxchv+99kLk5lt/+S5efP+fu/zJM9LEK+lskH3ET0y5XJNXHtsD15/YRya2lHZRu/LWrqZ/ZnF+4j/Snq+LMHhm/FEVrcwu/4QvjthOGLj6IoMQzvrRedUAF87/TtZwhr6rdSju/4BC2VNCZ7J6NK1tW1cWXsCy41R6K/bYQxboZ6jUT9lgjxghHRYiy+ZXqQWVM7s/oB845WAGZse8i5G4Vpag2PlEZquOnH2UjuUl9uhmGhD63wQ6sg6FMEYyp0z4JzrR1KGqCcOK/V96Z1BL2oDDC58twFVdBvOu79DdsmEzz0K8AarYVrexIXIrzgdfADSHsOHLV1IOd7w19uHyGRCYbC5Jf1tKHE5cfLbDVBzT9Fx4zEKByhk90oh9puhi/2Gc+x5ReA+ihzLeF/Vg3dkfCR9KvqGIJtbnhw2nMJnFj/KR9YgD/2Ccv8YDjMV4Ho00Cxu43z4GeTTmxD71lnB98jQziKRykNiIe82kVZVjaRmEunGAfAH7+Lk+AMIfNNIM0sg9PvjLytYWBp8CIF3FQXWW/ikcwbv1vGxl1vxnNgrliKxoQD7NG1xe9nQPUgCG8hxWnDM3Q/pxE9x6cvueK4fkGO6iSzNLI42DuNASe1zIllQtbWnrgyHOkbAMd8E13mb7eRHFriH3N4hZDImlHpW43AevYRs4wLyO6/iWIMbH5fWrRDvFSn70uUOcNQh5BgjyKdjIB0rKGaBkr47cbDAFkWaQYec7gVk6hwo1lxGEWUFSTEhYg+n6uD+cpWX2z4OnnEOpDkKHhNDsf0WW8vgWHxI0YjZEcU4qKtiBXKUtY+CVOiXjggp2ctVSEjOrdTn1VpxwjAJkXkeQnoBAmYRpWyRlgCyuihwzd0oMtdD0KWFpMW5lS2iQiz75qttTC1UzIta+yHpGYD44iBE9CAqmCs4Ywuj2h5GjW0OX9FjqDF4twqljcv/+qFSciU2npp6IbioRBlDQWxvxlmHG43WSTTR41Aa3DvZpbIQ8X/JOH7miwy+bJJf37op0ar+PGXo+OP0+Z6fm+igt1bvVr9+/2+3+y5yPUumAQAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource catI18N;
  private static com.google.gwt.resources.client.ImageResource catLists;
  private static com.google.gwt.resources.client.ImageResource catOther;
  private static com.google.gwt.resources.client.ImageResource catPanels;
  private static com.google.gwt.resources.client.ImageResource catPopups;
  private static com.google.gwt.resources.client.ImageResource catTables;
  private static com.google.gwt.resources.client.ImageResource catTextInput;
  private static com.google.gwt.resources.client.ImageResource catWidgets;
  private static com.google.gwt.resources.client.ImageResource gwtLogo;
  private static com.google.gwt.resources.client.ImageResource gwtLogoThumb;
  private static com.google.gwt.resources.client.ImageResource isepLogo;
  private static com.google.gwt.resources.client.ImageResource jimmy;
  private static com.google.gwt.resources.client.ImageResource jimmyThumb;
  private static com.google.gwt.resources.client.ImageResource loading;
  private static com.google.gwt.resources.client.ImageResource locale;
  private static com.google.gwt.resources.client.CssResource css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      catI18N(), 
      catLists(), 
      catOther(), 
      catPanels(), 
      catPopups(), 
      catTables(), 
      catTextInput(), 
      catWidgets(), 
      gwtLogo(), 
      gwtLogoThumb(), 
      isepLogo(), 
      jimmy(), 
      jimmyThumb(), 
      loading(), 
      locale(), 
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("catI18N", catI18N());
        resourceMap.put("catLists", catLists());
        resourceMap.put("catOther", catOther());
        resourceMap.put("catPanels", catPanels());
        resourceMap.put("catPopups", catPopups());
        resourceMap.put("catTables", catTables());
        resourceMap.put("catTextInput", catTextInput());
        resourceMap.put("catWidgets", catWidgets());
        resourceMap.put("gwtLogo", gwtLogo());
        resourceMap.put("gwtLogoThumb", gwtLogoThumb());
        resourceMap.put("isepLogo", isepLogo());
        resourceMap.put("jimmy", jimmy());
        resourceMap.put("jimmyThumb", jimmyThumb());
        resourceMap.put("loading", loading());
        resourceMap.put("locale", locale());
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'catI18N': return this.@pt.isep.cms.client.ShowcaseResources::catI18N()();
      case 'catLists': return this.@pt.isep.cms.client.ShowcaseResources::catLists()();
      case 'catOther': return this.@pt.isep.cms.client.ShowcaseResources::catOther()();
      case 'catPanels': return this.@pt.isep.cms.client.ShowcaseResources::catPanels()();
      case 'catPopups': return this.@pt.isep.cms.client.ShowcaseResources::catPopups()();
      case 'catTables': return this.@pt.isep.cms.client.ShowcaseResources::catTables()();
      case 'catTextInput': return this.@pt.isep.cms.client.ShowcaseResources::catTextInput()();
      case 'catWidgets': return this.@pt.isep.cms.client.ShowcaseResources::catWidgets()();
      case 'gwtLogo': return this.@pt.isep.cms.client.ShowcaseResources::gwtLogo()();
      case 'gwtLogoThumb': return this.@pt.isep.cms.client.ShowcaseResources::gwtLogoThumb()();
      case 'isepLogo': return this.@pt.isep.cms.client.ShowcaseResources::isepLogo()();
      case 'jimmy': return this.@pt.isep.cms.client.ShowcaseResources::jimmy()();
      case 'jimmyThumb': return this.@pt.isep.cms.client.ShowcaseResources::jimmyThumb()();
      case 'loading': return this.@pt.isep.cms.client.ShowcaseResources::loading()();
      case 'locale': return this.@pt.isep.cms.client.ShowcaseResources::locale()();
      case 'css': return this.@pt.isep.cms.client.ShowcaseResources::css()();
    }
    return null;
  }-*/;
}
