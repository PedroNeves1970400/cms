package pt.isep.cms.client;

import static com.google.gwt.i18n.client.LocalizableResource.*;

@GeneratedFrom("pt/isep/cms/client/ShowcaseShell.ui.xml")
@Generate(
  format = {"com.google.gwt.i18n.rebind.format.PropertiesFormat", },
  locales = {"default", }
)
public interface ShowcaseShellShowcaseShellUiBinderImplGenMessages extends com.google.gwt.i18n.client.Messages {

  @DefaultMessage("This is a GWT Project")
  @Key("mainLinkHomepage")
  String message1();

  @DefaultMessage("College Management System")
  @Key("mainTitle")
  String message2();

  @DefaultMessage("Project Application for ODSOFT and EDOM")
  @Key("mainSubTitle")
  String message3();

}
