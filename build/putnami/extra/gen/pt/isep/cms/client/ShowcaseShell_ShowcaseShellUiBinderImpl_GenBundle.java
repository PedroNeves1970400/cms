package pt.isep.cms.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.DoNotEmbed;
import com.google.gwt.resources.client.DataResource.MimeType;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.CssResource.Import;

public interface ShowcaseShell_ShowcaseShellUiBinderImpl_GenBundle extends ClientBundle {
  @Source("uibinder.pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl_GenCss_style.gss")
  ShowcaseShell_ShowcaseShellUiBinderImpl_GenCss_style style();

}
