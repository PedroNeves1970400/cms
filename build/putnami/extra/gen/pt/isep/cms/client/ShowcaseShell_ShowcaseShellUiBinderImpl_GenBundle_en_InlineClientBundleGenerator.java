package pt.isep.cms.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ShowcaseShell_ShowcaseShellUiBinderImpl_GenBundle_en_InlineClientBundleGenerator implements pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl_GenBundle {
  private static ShowcaseShell_ShowcaseShellUiBinderImpl_GenBundle_en_InlineClientBundleGenerator _instance0 = new ShowcaseShell_ShowcaseShellUiBinderImpl_GenBundle_en_InlineClientBundleGenerator();
  private void styleInitializer() {
    style = new pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl_GenCss_style() {
      public java.lang.String contentButton() {
        return "AO2VSVD-c-a";
      }
      public java.lang.String contentButtonSource() {
        return "AO2VSVD-c-b";
      }
      public java.lang.String contentButtons() {
        return "AO2VSVD-c-c";
      }
      public java.lang.String contentList() {
        return "AO2VSVD-c-d";
      }
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? (".AO2VSVD-c-f{padding:2px 0 2px 22px;background:white;text-align:left}.AO2VSVD-c-e{font-size:8pt;line-height:10pt}.AO2VSVD-c-p{padding:0 10px;border-bottom:1px solid #c3c3c3}.AO2VSVD-c-o{color:#7b8fae;font-size:16pt;font-weight:bold;text-shadow:#ddd 3px 3px 1px;margin:0;padding:0 4px 0 0}.AO2VSVD-c-n{color:#888;font-size:12pt;margin:0;padding:0 6px 0 0}.AO2VSVD-c-i{padding:6px 0 0 10px}.AO2VSVD-c-g{color:blue;font-size:8pt;margin-right:4px}.AO2VSVD-c-j{width:36px;height:16px;margin:3px 10px 0 0;padding:0}.AO2VSVD-c-m{background:#d0e4f6}.AO2VSVD-c-k{background:#ccc}.AO2VSVD-c-l{background:#3d3d3d}.AO2VSVD-c-h{background-color:#d7dde8;border-left:1px solid #c3c3c3}.AO2VSVD-c-c{background-color:#d7dde8;border-bottom:1px solid #c3c3c3;padding:0 10px}.AO2VSVD-c-a{margin-left:20px;color:#888;font-weight:bold;cursor:hand;cursor:pointer;line-height:20pt;vertical-align:middle}.AO2VSVD-c-a:hover{color:#4b4a4a;text-decoration:underline}.AO2VSVD-c-b{margin-left:4px}.AO2VSVD-c-d{font-size:8pt;color:#4b4a4a;direction:rtl}") : (".AO2VSVD-c-f{padding:2px 22px 2px 0;background:white;text-align:right}.AO2VSVD-c-e{font-size:8pt;line-height:10pt}.AO2VSVD-c-p{padding:0 10px;border-bottom:1px solid #c3c3c3}.AO2VSVD-c-o{color:#7b8fae;font-size:16pt;font-weight:bold;text-shadow:#ddd 3px 3px 1px;margin:0;padding:0 0 0 4px}.AO2VSVD-c-n{color:#888;font-size:12pt;margin:0;padding:0 0 0 6px}.AO2VSVD-c-i{padding:6px 10px 0 0}.AO2VSVD-c-g{color:blue;font-size:8pt;margin-left:4px}.AO2VSVD-c-j{width:36px;height:16px;margin:3px 0 0 10px;padding:0}.AO2VSVD-c-m{background:#d0e4f6}.AO2VSVD-c-k{background:#ccc}.AO2VSVD-c-l{background:#3d3d3d}.AO2VSVD-c-h{background-color:#d7dde8;border-right:1px solid #c3c3c3}.AO2VSVD-c-c{background-color:#d7dde8;border-bottom:1px solid #c3c3c3;padding:0 10px}.AO2VSVD-c-a{margin-right:20px;color:#888;font-weight:bold;cursor:hand;cursor:pointer;line-height:20pt;vertical-align:middle}.AO2VSVD-c-a:hover{color:#4b4a4a;text-decoration:underline}.AO2VSVD-c-b{margin-right:4px}.AO2VSVD-c-d{font-size:8pt;color:#4b4a4a;direction:ltr}");
      }
      public java.lang.String link() {
        return "AO2VSVD-c-e";
      }
      public java.lang.String linkBar() {
        return "AO2VSVD-c-f";
      }
      public java.lang.String localeBox() {
        return "AO2VSVD-c-g";
      }
      public java.lang.String mainMenu() {
        return "AO2VSVD-c-h";
      }
      public java.lang.String options() {
        return "AO2VSVD-c-i";
      }
      public java.lang.String styleSelectionButton() {
        return "AO2VSVD-c-j";
      }
      public java.lang.String styleSelectionChrome() {
        return "AO2VSVD-c-k";
      }
      public java.lang.String styleSelectionDark() {
        return "AO2VSVD-c-l";
      }
      public java.lang.String styleSelectionStandard() {
        return "AO2VSVD-c-m";
      }
      public java.lang.String subtitle() {
        return "AO2VSVD-c-n";
      }
      public java.lang.String title() {
        return "AO2VSVD-c-o";
      }
      public java.lang.String titleBar() {
        return "AO2VSVD-c-p";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl_GenCss_style get() {
      return style;
    }
  }
  public pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl_GenCss_style style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl_GenCss_style style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'style': return this.@pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl_GenBundle::style()();
    }
    return null;
  }-*/;
}
