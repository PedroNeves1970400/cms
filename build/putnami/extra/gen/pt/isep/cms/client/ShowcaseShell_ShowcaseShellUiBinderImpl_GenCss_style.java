package pt.isep.cms.client;

import com.google.gwt.resources.client.CssResource;

public interface ShowcaseShell_ShowcaseShellUiBinderImpl_GenCss_style extends CssResource {
  String localeBox();
  String styleSelectionDark();
  String mainMenu();
  String styleSelectionChrome();
  String contentButtons();
  String link();
  String title();
  String titleBar();
  String contentButton();
  String linkBar();
  String subtitle();
  String styleSelectionStandard();
  String options();
  String contentList();
  String styleSelectionButton();
  String contentButtonSource();
}
