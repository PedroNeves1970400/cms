package pt.isep.cms.client;

public class ShowcaseShell_ShowcaseShellUiBinderImpl_TemplateImpl implements pt.isep.cms.client.ShowcaseShell_ShowcaseShellUiBinderImpl.Template {
  
  public com.google.gwt.safehtml.shared.SafeHtml html1(java.lang.String arg0,java.lang.String arg1,com.google.gwt.safehtml.shared.SafeHtml arg2) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<table align='right' cellpadding='0' cellspacing='0' id='");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("'> <tr> <td class='");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
    sb.append("'> <a href='http://www.gwtproject.org/'> ");
    sb.append(arg2.asString());
    sb.append(" </a> </td> </tr> </table>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}

public com.google.gwt.safehtml.shared.SafeHtml html2(java.lang.String arg0,java.lang.String arg1,com.google.gwt.safehtml.shared.SafeHtml arg2,java.lang.String arg3,com.google.gwt.safehtml.shared.SafeHtml arg4,java.lang.String arg5,java.lang.String arg6,java.lang.String arg7,java.lang.String arg8) {
StringBuilder sb = new java.lang.StringBuilder();
sb.append("<table cellpadding='0' cellspacing='0' width='100%'> <tr> <td> <table cellpadding='0' cellspacing='0'> <tr> <td style='line-height:0px'> <span id='");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
sb.append("'></span> </td> <td> <h1 class='");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
sb.append("'> ");
sb.append(arg2.asString());
sb.append(" </h1> <h2 class='");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg3));
sb.append("'> ");
sb.append(arg4.asString());
sb.append(" </h2> </td> </tr> </table> </td> <td align='right' class='");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg5));
sb.append("' id='");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg6));
sb.append("' valign='top'> <table cellpadding='0' cellspacing='0'> <tr> <td valign='middle'> <span id='");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg7));
sb.append("'></span> </td> <td valign='middle'> <span id='");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg8));
sb.append("'></span> </td> </tr> </table> </td> </tr> </table>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
